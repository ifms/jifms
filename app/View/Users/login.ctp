<script type="text/javascript">
    $(document).ready(function(){
        $('#enviarRecoverForm').click(function(){
            $.ajax({
                url: '<?= $this->Html->url(array("controller" => "users", "action" => "recover")) ?>',
                type: 'POST',
                data: {login: $('#login').val(), email: $('#email').val()},
                success: function(data) {
                    $(".mensagem").html(data);
                }
            });
        });
    });
</script>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <img src="<?= $this->Html->url('/img/logoif.png'); ?>" style="width:100%; margin-top:10%; margin-bottom: 5%;">
            <br>
            <div class="login-panel panel panel-default" style="margin-top:0px !important;">

                <div class="panel-heading">
                    <h3 class="panel-title">Painel Administrativo - Acesso</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create('User', array('role' => 'form')); ?>
                        <fieldset>
                            <div class="form-group">
                                <?php echo $this->Form->input('username', array('autofocus' => "autofocus", 'type' => 'text', 'class' => 'form-control', 'label' => 'Login')); ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'label' => 'Senha')); ?>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div>
                            <?php echo $this->Form->submit('Login', array('class' => 'btn btn-lg btn-success btn-block')); ?><br>
                            <div class="text-center"><a href="#" data-toggle="modal" data-target="#myModal">Recuperar senha</a></div>
                        </fieldset>
                <?php echo $this->Form->end(); ?>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" style='z-index:2000' role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Recuperar senha</h4>
                      </div>
                      <div class="modal-body">
                        <form id='recoverForm' method="POST" action='<?= $this->Html->url(array("controller" => "users", "action" => "recover")) ?>'>
                            <p>Informe o e-mail cadastrado ou seu login para recuperar sua senha:</p>
                            <input name='email' id='email' onclick="$('#login').val('')" class="form-control" placeholder="Insira o email">
                            <p class="help-block text-center">ou</p>
                            <input name='login' id='login' onclick="$('#email').val('')" class="form-control" placeholder="Insira o login">
                        </form>
                        <div class='mensagem'></div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="enviarRecoverForm">Enviar</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>