<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeUsuarios + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cadastrar Usuário</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('User', array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control'
            )
        )); ?>
        <fieldset>
            Perfils que este usuário pode assumir:
            <?php
            foreach($roles as $key => $role){
                echo '<label class="checkbox">';
                echo $this->Form->checkbox('tipo', array(
                    'value' => $key,
                    'name' => 'data[User][perfil][]',
                    "hiddenField" => false
                ));
                echo $role . '</label>';
            }
            echo $this->Form->input('fullname', array(
                'label' => 'Informe um nome', 
                'id' => 'nome'
            ));
            echo $this->Form->input('email', array(
                'label' => 'Informe um email', 
                'id' => 'email'
            ));
            ?>
        </fieldset>    
        <div class="alert alert-info">
            Informe um email correto, para enviarmos seu username (login) e sua senha pelo email informado acima.
        </div>
        <?php
        echo $this->Form->submit('Cadastrar', array(
            'name' => 'submit', 
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary'
        ));
        echo $this->Form->end();
        ?>
    </div>
</div>