<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Visualizar Usuario</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <div class="btn-group">
                <a href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span> Alterar </a>
                <?php if ($user['User']['active']) { ?>
                    <a href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'disable', $user['User']['id'])); ?>' class='btn btn-warning'><span class='glyphicon glyphicon-remove-circle'></span> Inativar </a>
                <?php } else { ?>
                    <a href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'enable', $user['User']['id'])); ?>' class='btn btn-success'><span class='glyphicon glyphicon-check'></span> Ativar </a>
                <?php } ?>
                <?= $this->Form->postLink('Deletar', array('action' => 'delete', $this->params['pass'][0]), array('confirm' => 'Você tem certeza?', 'class' => 'btn btn-danger')); ?>
            </div>
            <br><br>
            <table class="table">
                <tr>
                    <th> Nome </th>
                    <td> <?= $user['User']['fullname']; ?> </td>
                </tr>    
                <tr>
                    <th> Username (Nome de Usuário) </th>
                    <td> <?= $user['User']['username']; ?> </td>
                </tr>
                <tr>
                    <th> Senha </th>
                    <td> *** <a href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>' class='btn btn-success'><span class='glyphicon glyphicon-edit'></span> Alterar Senha </a> </td>
                </tr>
                <tr>
                    <th> Ativo </th>
                    <td> <?= $user['User']['active'] == 1 ? "SIM" : "NAO"; ?> </td>
                </tr>
                <tr>
                    <th> Tipo Usuario </th>
                    <td> <?= $user['User']['role']; ?> </td>
                </tr>
            </table>
        </div>
    </div>
</div>