<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeUsuarios + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<?php
	echo $this->Html->css('ui-tree/ui.dynatree');
    echo $this->fetch('css');
    echo $this->Html->script('jquery-ui.min');
	echo $this->Html->script('jquery.dynatree.min');
    echo $this->fetch('script');
?>
<?php //print_r($accessTree); exit();?>
<script type="text/javascript">
	$(document).ready(function(){
		<?php foreach($accessTree as $role => $tree) : ?>
			$('#roleId<?= $role ?>').fancytree({
		        source: JSON.parse('<?= json_encode($tree) ?>'),
		        selectMode: 3,
		        checkbox: true,
		        activate: function(e, data){
		            $("div#statusLine").text("Active node: " + data.node);
		        },
		        select: function(event, data) {
			        var selRootNodes = data.tree.getSelectedNodes(true);
			        // ... and convert to a key array:
			        var selRootKeys = $.map(selRootNodes, function(node){
			          return node.key;
			        });
			        var form = $('#formRole<?= $role ?>');
			        $(form).find('input.checkbox').remove();
			        for(var i = 0; i < selRootKeys.length; i++) {
			        	var input = '<input type="hidden" class="checkbox" name="access[]" ';
			        	input += 'value="' + selRootKeys[i] + '">';
			        	$(form).append(input);
			        }
			     }
		    });
		<?php endforeach; ?>
	});
</script>
<div class="row">
	<h1 class="page-header">Gerenciar Acesso</h1>
	<?php foreach ($roles as $i => $role) : ?>
	<div class="col-md-4 col-sm-6">
		<h2><?= $role['Aro']['alias'] ?></h2>	
		<div id="roleId<?= $role['Aro']['id'] ?>" form-role="<?= $role['Aro']['id'] ?>"></div><br>
		<form id="formRole<?= $role['Aro']['id'] ?>" method="POST">
			<input name='aroId' value='<?= $role['Aro']['id'] ?>' type='hidden'>
			<input name='aroAlias' value='<?= $role['Aro']['alias'] ?>' type='hidden'>
			<input type='submit' class='btn btn-success btn-sm' value="Salvar">
		</form>
	</div>
	<?= (($i+1)%3==0) ? '<div class="clearfix"></div>' : "" ?>
	<?php endforeach; ?>
</div>
