<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeUsuarios + ul").children('li')[3]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Alterar Senha</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('User', array(
            'type' => 'post',
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control',
                'type' => 'password',
                'required' => true
            )
        )); 
        echo $this->Form->input('senha_atual', array(
            'label' => 'Digite a senha atual'
        ));
        echo $this->Form->input('senha_nova', array(
            'label' => 'Digite a nova senha'
        ));
        echo $this->Form->input('senha_repetir', array(
            'label' => 'Repita a senha'
        ));
        echo $this->Form->submit('Alterar', array(
            'class' => 'btn btn-primary'
        ));
        echo $this->Form->end();

        ?>
    </div>
</div>
</script>

</script>