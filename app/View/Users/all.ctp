<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeUsuarios + ul").children('li')[2]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Usuários</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="btn-group">
            <a href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'cadastrarUsuario')); ?>' class="btn btn-success">Novo Usuario</a>
        </div>
        <br><br>
        <p> 
            Abaixo segue a lista de usuários cadastrados no sistema. 
        </p>
        <div class="table-responsive">
            <table class="table">
                <tr>
                    <th> Nome </th>
                    <th> Nome de Usuário (Username) </th>
                    <th> Senha </th>
                    <!--<th> </th>
                    <th> </th>-->
                </tr>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= $user['User']['fullname']; ?></td>
                        <td><?= $user['User']['username']; ?></td>
                        <td>****</td>
                        <!--<td style="width:100px"><a style="width:130px" href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'edit', $user['User']['id'])); ?>' class='btn-sm btn btn-primary'><span class='glyphicon glyphicon-edit'></span> Alterar </a></td>
                        <td style="width:100px"><a style="width:130px" href='<?= $this->Html->url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>' class='btn-sm btn btn-primary'><span class='glyphicon glyphicon-book'></span> Visualizar </a></td>-->
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <ul class="pagination">
            <?php
            echo "<li>" . $this->Paginator->prev('«', null, null, array('class' => 'disabled')) . "</li>";
            echo "<li>" . $this->Paginator->numbers(array('separator' => '', 'class' => '')) . "</li>";
            echo "<li>" . $this->Paginator->next('»', null, null, array('class' => 'disabled')) . "</li>";
            ?>
        </ul>
    </div>
</div>