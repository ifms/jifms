<script>
    $("#gerenciaDeUsuarios").parent('li').toggleClass('active').children('ul').collapse('toggle');
</script>


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Recuperar Senha - Usuário</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('User', array('type' => 'post')); ?>
        <fieldset>
            <?php
            echo $this->Form->input('username', array('label' => 'Informe um nome de Usuário', 'id' => 'username', 'class' => 'form-control'));
            echo $this->Form->input('email', array('label' => 'Informe um email', 'id' => 'email', 'class' => 'form-control'));
            echo "<label for='newsenha'> Alterar Senha: </label><input type='password' name='newsenha' id='newsenha' class='form-control'>";
            ?>
            <?php
            $options = array('user' => 'Usuário', 'adm' => 'Administrador');
            echo $this->Form->input('role', array('type' => 'hidden', 'class' => 'form-control', 'label' => 'Tipo de usuário'
            ));
            ?>
        </fieldset>    
        <br>
        <?php
        echo $this->Form->submit('Editar', array('name' => 'submit', 'id' => 'btn_submitCadastro', 'class' => 'btn btn-primary'));
        echo $this->Form->end();        
        ?>
    </div>
</div>
</script>

</script>