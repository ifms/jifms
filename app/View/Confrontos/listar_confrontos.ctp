<script>
    $("#gerenciaDeConfrontos").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeConfrontos + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Confrontos</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-success">
  <div id="sub-menu" class="panel-heading">Ações do Módulo <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="sub-menu-panel" style="display:block;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $this->Html->url(array('action' => 'cadastrarConfronto')); ?>">Cadastrar Confronto</a>
        </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<hr class="divider">
<div class="row">
    <div class="col-lg-12">
    <?php 
        $form = $this->Form;
        $inputDefaults = array(
            'class' => 'input-sm form-control',
            'label' => false,
            'required' => true
        );
        foreach ($modalidades as $i => $modalidade): ?>
        <?php if (!empty($modalidade['Confrontos'])): ?>
            <h1> <?= $modalidade['Modalidade']['name'] ?> </h1>
        <table class="table">
            <tr>
                <th> # </th>
                <th width='100'> Data </th>
                <th width='80'> Hora </th>
                <th class='text-right'> Delegação A </th>
                <th width='80'>  </th>
                <th width='30'>  </th>
                <th width='80'>  </th>
                <th> Delegação B </th>
                <th width='140'>  </th>
                <th width='140'>  </th>
            </tr>
            <?php foreach ($modalidade['Confrontos'] as $j => $confronto): ?>
            <tr>
                <?= $form->create('Confronto', array('inputDefaults' => $inputDefaults)) ?>
                <td> <?= $confronto['Confronto']['numero'] ?> </td>
                <td> <?= date('d/m/Y', strtotime($confronto['Confronto']['data'])) ?> </td>
                <td> <?= date('H:i', strtotime($confronto['Confronto']['data'])) ?> </td>
                <td class='text-right'> <?= $confronto['Confronto']['delegacao_a'] ?> </td>
                <td> <?= $form->input('pontuacao_a'); ?> </td>
                <td> X </td>
                <td> <?= $form->input('pontuacao_b'); ?> </td>
                <td> <?= $confronto['Confronto']['delegacao_b'] ?> </td>
                <td> <?= $form->submit('Lançar Resultado', array('class' => 'btn btn-sm btn-block btn-success')) ?> </td>
                <td> <?= $this->Form->postLink('Deletar', array('action' => 'deletarConfronto', $confronto['Confronto']['id']), array('class' => 'btn btn-sm btn-danger btn-block')) ?> </td>
                <?= $form->end(); ?>
            </tr>
            <?php endforeach ?>
        </table>
        <?php endif ?>
    <?php endforeach ?>

    </div>
</div>