<script type="text/javascript">
    <?php foreach ($delegacoes as $i => $delegacao): ?>
        <?= "delegacoes[{$i}] = {id:{$delegacao['Delegacao']['id']}, nome:'{$delegacao['Delegacao']['nome']}'};" ?>
    <?php endforeach ?>
</script>
<?php
    $form = $this->Form;
    echo $form->Create('Confronto', array(
        'url' => array(
            'controller' => 'confrontos',
            'action' => 'cadastrarConfronto'
        ),
        'class' => 'form-inline',
        'inputDefaults' => array(
            'div' => 'form-group',
            'class' => 'form-control input-sm',
            'label' => false
        )
    ));
?>
<?php if (empty($revezamentoGrupos['grupoA'])): ?>

    <div class="col-md-12 confrontoContainer">

        <div id="configuracoes">
            <div class="btn-group pull-right" style="margin: 9px 0 5px;">
                <button type="button" class="btn btn-sm disabled">
                    <span class="glyphicon glyphicon-cog"></span>
                </button>
                <button type="button" id='cadastroPadrao' class="btn btn-sm btn-default">Padrão</button>
                <button type="button" id='cadastroManual' class="btn btn-sm btn-default">Manual</button>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="list-group">
            <a class="list-group-item" style="display:block;width:568px;font-weight:bold">
                <div style="margin: 0 3px;display:inline-block;">#</div>
                <div style="margin: 0 3px;display:inline-block;width: 100px;"> Data </div>
                <div style="margin: 0 3px;display:inline-block;width: 65px;">Hora</div>
                <div style="margin: 0 3px;display:inline-block;width: 140px;">Time A</div>
                <div style="margin: 0 3px;display:inline-block;width: 150px;">Time B</div>
            </a>
        </div>
        <div class="sortable" class="list-group">
            <?php $i = 1; ?>
            <?php if (!empty($revezamento)): ?>
                <?php foreach ($revezamento as $key => $confronto): ?>
                <a class="list-group-item">
                <?php 
                    echo $form->input('Confronto.' . $key . '.modalidade_id', array(
                        'type' => 'hidden',
                        'value' => $modalidadeId
                    ));
                    echo $form->input('Confronto.' . $key . '.numero', array(
                        'div' => 'form-group numero',
                        'style' => 'display:none',
                        'type' => 'text',
                        'label' => $i . 'º',
                        'value' => $i++,
                    ));
                    echo $form->input('Confronto.' . $key . '.data_form', array(
                        'class' => 'form-control input-sm data_form',
                        'placeholder' => date('d/m/Y'),
                        'value' => '12/06/2014',
                        'style' => 'width:100px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $key . '.hora_form', array(
                        'class' => 'form-control input-sm hora_form',
                        'placeholder' => date('H:m'),
                        'value' => '12:00',
                        'style' => 'width:65px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $key . '.delegacao_a', array(
                        'options' => array($confronto[0]['id'] => $confronto[0]['nome']),
                        'readonly' => true
                    ));
                    echo $form->input('Confronto.' . $key . '.delegacao_b', array(
                        'options' => array($confronto[1]['id'] => $confronto[1]['nome']),
                        'readonly' => true
                    )); 
                ?>
                    <p class="btn btn-xs btn-danger deletarConfronto"><span class="glyphicon glyphicon-remove"></span></p>
                </a>
                <?php endforeach ?>
            <?php else: ?>
                <div class="list-group text-center" style='width:568px'>
                    <h2>Nenhuma delegação está cadastrada nessa modalidade</h2>
                </div>      
            <?php endif ?>
        </div>
        <div class="list-group" style='width:568px'>
            <div class="list-group-item list-not-selectable">
                <p class='pull-right btn btn-sm btn-success adicionarConfronto'> Adicionar Novo Confronto </p>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

<?php else: ?>

    <div class="col-md-12 confrontoContainer" grupo-id='A'>

        <div id="configuracoes">
            <div class="btn-group pull-right" style="margin: 9px 0 5px;">
                <button type="button" class="btn btn-sm disabled">
                    <span class="glyphicon glyphicon-cog"></span>
                </button>
                <button type="button" id='cadastroPadrao' class="btn btn-sm btn-default">Padrão</button>
                <button type="button" id='cadastroManual' class="btn btn-sm btn-default">Manual</button>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="list-group">
            <a class="list-group-item" style="display:block;width:568px;font-weight:bold">
                <div style="margin: 0 3px;display:inline-block;">#</div>
                <div style="margin: 0 3px;display:inline-block;width: 100px;"> Data </div>
                <div style="margin: 0 3px;display:inline-block;width: 65px;">Hora</div>
                <div style="margin: 0 3px;display:inline-block;width: 140px;">Time A</div>
                <div style="margin: 0 3px;display:inline-block;width: 150px;">Time B</div>
            </a>
        </div>
        <div class="sortable" class="list-group">
            <?php $i = 1; $j = 0; ?>
                <?php foreach ($revezamentoGrupos['grupoA'] as $key => $confronto): ?>
                <a class="list-group-item">
                <?php 
                    echo $form->input('Confronto.' . $j . '.modalidade_id', array(
                        'type' => 'hidden',
                        'value' => $modalidadeId
                    ));
                    echo $form->input('Confronto.' . $j . '.grupo', array(
                        'type' => 'hidden',
                        'value' => 'A'
                    ));
                    echo $form->input('Confronto.' . $j . '.numero', array(
                        'div' => 'form-group numero',
                        'style' => 'display:none',
                        'type' => 'text',
                        'label' => $i . 'º',
                        'value' => $i++,
                    ));
                    echo $form->input('Confronto.' . $j . '.data_form', array(
                        'class' => 'form-control input-sm data_form',
                        'placeholder' => date('d/m/Y'),
                        'value' => '19/06/2014',
                        'style' => 'width:100px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $j . '.hora_form', array(
                        'class' => 'form-control input-sm hora_form',
                        'placeholder' => date('H:m'),
                        'value' => '12:00',
                        'style' => 'width:65px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $j . '.delegacao_a', array(
                        'options' => array($confronto[0]['id'] => $confronto[0]['nome']),
                        'readonly' => true
                    ));
                    echo $form->input('Confronto.' . $j++ . '.delegacao_b', array(
                        'options' => array($confronto[1]['id'] => $confronto[1]['nome']),
                        'readonly' => true
                    )); 
                ?>
                    <p class="btn btn-xs btn-danger deletarConfronto"><span class="glyphicon glyphicon-remove"></span></p>
                </a>
                <?php endforeach ?>
        </div>
        <div class="list-group" style='width:568px'>
            <div class="list-group-item list-not-selectable">
                <p class='pull-right btn btn-sm btn-success adicionarConfronto'> Adicionar Novo Confronto </p>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>

    <div class="col-md-12 confrontoContainer" grupo-id='B'>

        <div id="configuracoes">
            <div class="btn-group pull-right" style="margin: 9px 0 5px;">
                <button type="button" class="btn btn-sm disabled">
                    <span class="glyphicon glyphicon-cog"></span>
                </button>
                <button type="button" id='cadastroPadrao' class="btn btn-sm btn-default">Padrão</button>
                <button type="button" id='cadastroManual' class="btn btn-sm btn-default">Manual</button>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="list-group">
            <a class="list-group-item" style="display:block;width:568px;font-weight:bold">
                <div style="margin: 0 3px;display:inline-block;">#</div>
                <div style="margin: 0 3px;display:inline-block;width: 100px;"> Data </div>
                <div style="margin: 0 3px;display:inline-block;width: 65px;">Hora</div>
                <div style="margin: 0 3px;display:inline-block;width: 140px;">Time A</div>
                <div style="margin: 0 3px;display:inline-block;width: 150px;">Time B</div>
            </a>
        </div>
        <div class="sortable" class="list-group">
            <?php $i = 1; ?>
                <?php foreach ($revezamentoGrupos['grupoB'] as $key => $confronto): ?>
                <a class="list-group-item">
                <?php 
                    echo $form->input('Confronto.' . $j . '.modalidade_id', array(
                        'type' => 'hidden',
                        'value' => $modalidadeId
                    ));
                    echo $form->input('Confronto.' . $j . '.grupo', array(
                        'type' => 'hidden',
                        'value' => 'B'
                    ));
                    echo $form->input('Confronto.' . $j . '.numero', array(
                        'div' => 'form-group numero',
                        'style' => 'display:none',
                        'type' => 'text',
                        'label' => $i . 'º',
                        'value' => $i++,
                    ));
                    echo $form->input('Confronto.' . $j . '.data_form', array(
                        'class' => 'form-control input-sm data_form',
                        'placeholder' => date('d/m/Y'),
                        'value' => '19/06/2014',
                        'style' => 'width:100px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $j . '.hora_form', array(
                        'class' => 'form-control input-sm hora_form',
                        'placeholder' => date('H:m'),
                        'value' => '12:00',
                        'style' => 'width:65px',
                        'required' => true
                    ));
                    echo $form->input('Confronto.' . $j . '.delegacao_a', array(
                        'options' => array($confronto[0]['id'] => $confronto[0]['nome']),
                        'readonly' => true
                    ));
                    echo $form->input('Confronto.' . $j++ . '.delegacao_b', array(
                        'options' => array($confronto[1]['id'] => $confronto[1]['nome']),
                        'readonly' => true
                    )); 
                ?>
                    <p class="btn btn-xs btn-danger deletarConfronto"><span class="glyphicon glyphicon-remove"></span></p>
                </a>
                <?php endforeach ?>
        </div>
        <div class="list-group" style='width:568px'>
            <div class="list-group-item list-not-selectable">
                <p class='pull-right btn btn-sm btn-success adicionarConfronto'> Adicionar Novo Confronto </p>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    
<?php endif ?>

<div class="clearfix"></div>

<?= $form->submit('Salvar', array('class' => 'btn btn-primary')); ?>
<?=  $form->end(); ?>