<?php $form = $this->Form; ?>
<style type="text/css">
    .sortable {
        display: inline-block;
        width: 568px
    }
    #configuracoes {
        width: 568px
    }
    .placeholder_table {
        position: relative;
        display: block;
        padding: 0;
        height: 41px;
        margin: 5px;
        border: 1px dashed #CCC;
        background: #EFE !important;
    }
    .placeholder_table:after {
        content: "Solte aqui";
        display: block;
        text-align: center;
        color: #AAA;
        padding: 10px;
    }
    #confrontos label {
        margin: 0 3px;
    }
    #confrontos input, #confrontos select {
        margin: 0 3px;
        width: 150px
    }
    #confrontos a {
        cursor:all-scroll;
    }
</style>

<script>
    var delegacoes = new Array();
    $("#gerenciaDeConfrontos").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeConfrontos + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
    function updateNumero() {
        $('.numero').each(function(i, e){
            $(this).find('input').val(i+1);
            $(this).find('label').html((i+1)+'º');
        });
    }
    $(document).ready(function(){
        $('#parent_id').change(function(){
            if ($(this).val() != '') {
                sistema.showLoading();
                $.ajax({
                    url: '<?= $this->Html->url(array('controller' => 'confrontos', 'action' => 'construirConfrontos')) ?>/' + $(this).val(),
                    type: 'POST',
                    success: function(data) {
                        $('#confrontos').html(data);
                        $( ".sortable" ).sortable({
                            placeholder: 'placeholder_table',
                            stop: updateNumero
                        });
                        $('.hora_form').mask('99:99');
                        $('.data_form').mask('99/99/9999');
                    },
                    complete: function() {
                        sistema.hideLoading();
                    }
                })
            }
        });
        $(document).on('click', '#cadastroManual', function(){
            $('#confrontos select').removeAttr('readonly');
            $('#confrontos select').html('');
            for (var i = 0; i < delegacoes.length; i++) {
                var option = document.createElement('option');
                 $(option).val(delegacoes[i]['id']);
                 $(option).html(delegacoes[i]['nome']);
                 $('#confrontos select').append(option);
            }
        });
        $(document).on('click', '#cadastroPadrao', function(){
            $('#parent_id').change();
        });
        function isUnique(random) {
            var n = $(".numero input").map(function(){
                return $(this).attr('name').substr(16,1);
            }).get();
            for (var i = 0; i < n.length; i++) {
                if(n[i] == random) return false;
            };
            return true;
        }
        $(document).on('click', '.adicionarConfronto', function(){
            var r = Math.floor(Math.random() * 100) + 1;
            while(!isUnique(r)) {
                r = Math.floor(Math.random() * 100) + 1;
            }

            var listGroup = document.createElement('a');
            $(listGroup).addClass('list-group-item');

            var idModalidade = $('#parent_id').val();

            var inputModalidadeId = '<input type="hidden" name="data[Confronto]['+r+'][modalidade_id]" value="'+idModalidade+'"/>';
            $(listGroup).append(inputModalidadeId);
            var nomeGrupo = $(this).parents('.confrontoContainer').attr('grupo-id');
            var grupo = '<input type="hidden" name="data[Confronto]['+r+'][grupo]" value="'+nomeGrupo+'"/>';
            $(listGroup).append(grupo);

            var numero = $(this).parents('.confrontoContainer').find('.numero').size() + 1;
            var inputNumero = '<div class="form-group numero"><label for="Confronto'+r+'Numero">'+numero+'º</label><input type="hidden" class="form-control" name="data[Confronto]['+r+'][numero]" value="'+numero+'" id="Confronto'+r+'Numero"/></div>';
            $(listGroup).append(inputNumero);

            var inputData = '<div class="form-group"><input class="form-control input-sm data_form" placeholder="<?= date('d/m/Y') ?>" style="width:100px" required="required" name="data[Confronto]['+r+'][data_form]" type="text" id="Confronto'+r+'DataForm"/></div>';
            $(listGroup).append(inputData);

            var inputHora = '<div class="form-group"><input class="form-control input-sm hora_form" placeholder="<?= date('H:m') ?>" style="width:65px" required="required" name="data[Confronto]['+r+'][hora_form]" type="text" id="Confronto'+r+'HoraForm"/></div>';
            $(listGroup).append(inputHora);

            var select = document.createElement('select');
            $(select).addClass('form-control');
            $(select).addClass('input-sm');
            $(select).attr('id', 'Confronto'+r+'DelegacaoA');
            $(select).attr('name', 'data[Confronto]['+r+'][delegacao_a]');
            for (var i = 0; i < delegacoes.length; i++) {
                var option = document.createElement('option');
                 $(option).val(delegacoes[i]['id']);
                 $(option).html(delegacoes[i]['nome']);
                 $(select).append(option);
            }
            var divFormGroup = document.createElement('div');
            $(divFormGroup).addClass('form-group');
            var selectTimeA = $(divFormGroup).html(select);
            $(listGroup).append(selectTimeA);

            var select2 = document.createElement('select');
            $(select2).addClass('form-control');
            $(select2).addClass('input-sm');
            $(select2).attr('id', 'Confronto'+r+'DelegacaoB');
            $(select2).attr('name', 'data[Confronto]['+r+'][delegacao_b]');
            for (var i = 0; i < delegacoes.length; i++) {
                var option = document.createElement('option');
                 $(option).val(delegacoes[i]['id']);
                 $(option).html(delegacoes[i]['nome']);
                 $(select2).append(option);
            }
            var divFormGroup = document.createElement('div');
            $(divFormGroup).addClass('form-group');
            var selectTimeB = $(divFormGroup).html(select2);
            $(listGroup).append(selectTimeB);

            $(listGroup).append(' <p class="btn btn-xs btn-danger deletarConfronto"><span class="glyphicon glyphicon-remove"></span></p>');

            $(this).parents('.confrontoContainer').find('.sortable').append(listGroup);  
            $('.hora_form').mask('99:99');
            $('.data_form').mask('99/99/9999');
            $( ".sortable" ).sortable('refresh');
        });
        $(document).on('click', '.deletarConfronto', function(){
            var sim = confirm('Deseja deletar confronto?');
            if(sim) $(this).parent('.list-group-item').remove();
            updateNumero();
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cadastrar Confronto</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    <?php 
        echo $form->create('Modalidade', array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control'
            )
        ));
        echo $form->input('parent_id', array(
            'label' => 'Selecione a modalidade', 
            'id' => 'parent_id', 
            'options' => $this->request->data['Modalidade']['parent_id'],
            'empty' => 'Selecione a Categoria (Opcional).'
        ));
        echo $form->submit('Cadastrar', array(
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary form-group'
        ));
        echo $form->end();
        ?>	
    </div>
    <div id="confrontos">


    </div>
</div>