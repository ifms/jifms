<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar Servidor</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    	<?php
            $form = $this->Form;

    		echo $form->create('Servidor', array(
                'type' => 'post',
                'enctype' => 'multipart/form-data',
    			'inputDefaults' => array(
    				'div' => 'form-group',
    				'class' => 'form-control'
    				),
                'url' => array('controller' => 'delegacoes', 'action' => 'editarServidor', $this->request->data['Servidor']['id']),
                'type' => 'post'
    			)
    		);

            echo $form->input('id', array(
                'type' => 'hidden',
                'value' => $this->request->data['Servidor']['id']
            ));
    		?>
            <label> Campus: </label>
            <select name='data[Servidor][campu_id]' id='campu_id' class="form-control">
              <?php foreach($campu_id_opcoes as $key => $campu_op): 
              ?>
              <option value="<?= $key ?>" <?= $this->request->data['Servidor']['campu_id'] == $key ? "selected" : "" ?> > <?= $campu_op; ?> </option>
              <?php endforeach; ?>
            </select>
            <br>
            <?php
    		echo '<label class="checkbox" id="usuario">';
            $isChecked = !empty($this->request->data['Servidor']['user_id']) ? 'checked' : '';
    		echo $form->checkbox('usuario', array($isChecked));
            $userNameMapped = !empty($this->request->data['User']['fullname']) ? '<span class="badge">'.$this->request->data['User']['fullname'].'</span>' : "" ;
    		echo 'Servidor usuário do sistema '.$userNameMapped.' </label>';
    		$nomeInput = $form->input('nome', array(
    			'div' => array('class' => 'form-group nome_email'), 
    			'required' => true
            ));
    		echo $nomeInput;
    		$emailInput = $form->input('email', array(
    			'div' => array('class' => 'form-group nome_email'), 
    			'required' => true
            ));
            echo $emailInput;
    		echo $form->input('telefone', array(
                'required' => true
            ));
            echo $form->input('funcao', array(
                'required' => true
            ));
            echo $form->input('sexo', array('options' => array(
                'M' => 'Masculino',
                'F' => 'Feminino'
            )));
            ?>

            <div class="form-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      Clique para cadastrar dados adicionais
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <?php
                        echo '<label class="checkbox">';
                        echo $form->checkbox('jif');
                        echo 'JIF </label>';
                        echo $form->input('nascimento_form', array('label' => 'Nascimento', 'value' => date('d/m/Y', strtotime($this->request->data['Servidor']['nascimento']))));
                        echo $form->input('siape');
                        echo $form->input('cpf');
                        echo $form->input('rg');
                        echo $form->input('rg_expedidor', array('label' => 'Orgão Expedidor'));
                        echo $form->input('tipo_sanguineo', array('label' => 'Tipo Sanguíneo', 'options' => array(
                            '' => 'Selecione o Tipo Sanguíneo',
                            'A+' => 'A+',
                            'B+' => 'B+',
                            'AB+' => 'AB+',
                            'O-' => 'O-',
                            'A-' => 'A-',
                            'B-' => 'B-',
                            'AB-' => 'AB-',
                            'O+' => 'O+'
                        )));
                        echo $form->input('alergia');
                        echo $form->input('medicamentos');
                        echo $form->input('doenca_cronica', array('label' => 'Doenças Crônicas'));
                        echo $form->input('observacoes', array('label' => 'Observações'));
                        echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Foto (opcional)', 'id' => 'imagem', 'class' => 'form-control'));
                    ?>
                  </div>
                </div>
            </div>
            
        <?php
    		echo $form->submit('Editar',array('class' => 'btn btn-primary'));
    		echo $form->end();
    	?>
    </div>
</div>

<script>
	$(document).ready(function(){
	    $(document).on('change', '#ServidorUsuarioSelect', function(){
	    	console.log($(this).val());
	    	if($(this).val() != "") {
		    	$('#ServidorNome').val($(this).find(':selected').html());
		    	$('#ServidorEmail').val($(this).find(':selected').attr('email-usuario'));
	    	} else {
	    		$('#ServidorNome').val('');
		    	$('#ServidorEmail').val('');
	    	}
	    });
	    $('#ServidorUsuario').click(function(){
	    	var selected = [];
			$('#usuario input:checked').each(function() {
			    selected.push($(this).attr('name'));
			});
	    	if(selected[0] == "data[Servidor][usuario]") {
	    		$('.nome_email').remove();
	    		$('#usuario').after(
	    			'<?= $usuarios ?>' + 
	    			'<?= 
	    			$form->input("Servidor.nome", array("div" => array("class" => "form-group nome_email_input"), "id" => "ServidorNome", "readonly" => true)) .
	    			$form->input("Servidor.email", array("div" => array("class" => "form-group nome_email_input"), "id" => "ServidorEmail", "readonly" => true)) 
	    			?>'
	    		);
	    	} else {
	    		$('.nome_email_input').remove();
	    		$('.nome_email_server').remove();
	    		$('#usuario').after('<?= $nomeInput . $emailInput?>');
	    	}
	    });
	    $('#ServidorNascimentoForm').mask('99/99/9999');
	});
</script>