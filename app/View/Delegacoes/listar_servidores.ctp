<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});

    $(document).ready(function(){
        $("#buscas").click(function(){
            $("#buscas-panel").slideToggle('fast');
        });

        $("#sub-menu").click(function(){
            $("#sub-menu-panel").slideToggle('fast');
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Servidores</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-success">
  <div id="sub-menu" class="panel-heading">Ações do Módulo <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="sub-menu-panel" style="display:block;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $this->Html->url(array('action' => 'cadastrarServidor')); ?>">Cadastrar Servidor</a>
            |
            <a href="<?= $this->Html->url(array('action' => 'relatorioServidoresExcel')); ?>">Relatório de Servidores (exportar .xls)</a>
            |
            <a href="<?= $this->Html->url(array('controller' => 'crachas' ,'action' => 'gerarCrachaServidorLote')); ?>">Gerar Crachás Servidores (Lote)</a>
            |
            <a href="" data-target="#myModal" data-toggle="modal">Gerar Crachá Individual</a>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Gerar Crachá Individual Servidor</h4>
      </div>
      <div class="modal-body">
        <form action='<?= $this->Html->url("/crachas/gerarCrachaServidor"); ?>' method="post">
            <fieldset>
                <label>Servidor:</label><br>
                <select name="servidor_modal" id="servidor_modal" class="form-control">
                    <option value selected> Selecione um Servidor </option>
                    <?php
                        foreach ($servidores as $keyModal => $modModal) {
                            echo "<option value='".$modModal['Servidor']['id']."'> ".$modModal['Servidor']['nome']. "</option>";
                        }
                    ?>
                </select>
                <br>
                <button id="submitModal" type="submit" class="btn btn-primary"> Gerar Crachá </button>
            </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-info">
  <div id="buscas" class="panel-heading">Buscas <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="buscas-panel" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            Buscar Servidor por <b>Nome</b>
        </div>
        <div class="col-md-6">
            Buscar Servidor por <b>CPF</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_nome" id="termobusca_nome">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="termobusca_cpf" id="termobusca_cpf">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Buscar Servidor por <b>Câmpus</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_campus" id="termobusca_campus">
        </div>
        <div class="col-md-6">
            <input value="Buscar em filtros" type="submit" class="btn btn-primary btn-block">
        </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<hr class="divider">
<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th> <!-- Foto Servidor --> </th>
                <th>Nome do Servidor</th>
                <th>Campus</th>
                <th width='100'></th>
                <th width='100'></th>
                <th width='100'></th>
            </tr>
            <?php foreach ($servidores as $key => $servidor):
            ?>
            <tr>
                <td>
                    <?php $foto = empty($servidor['Servidor']['imagem']) ? "no-user.jpg" : "servidores/".$servidor['Servidor']['imagem']; ?>
                    <img src="<?= $this->Html->url('/img/' .  $foto); ?>" width="50" height="50" alt="foto_servidor">
                </td>
                <td><?= $servidor['Servidor']['nome']; ?></td>
                <td><?= $servidor['Campus']['nome']; ?></td>
                <td> <a href='<?= $this->Html->url(array("action" => "editarServidor", $servidor["Servidor"]["id"])); ?>' class="btn btn-block btn-sm btn-primary"> Alterar </a> </td>
                <td> <a href='<?= $this->Html->url(array("action" => "visualizarServidor", $servidor["Servidor"]["id"])); ?>' class="btn btn-block btn-sm btn-success"> Visualizar </a> </td>
                <td><?php echo $this->Form->postLink("Deletar", array('controller' => 'delegacoes', 'action' => 'deletarServidor', $servidor['Servidor']['id']), array('class' => 'btn btn-block btn-sm btn-danger'), "Tem certeza?") ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>