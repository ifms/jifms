<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Visualizar Delegação</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <tr>
                <td colspan="3">Informações sobre a delegação</td>
            </tr>
            <tr>
                <th>Nome Delegação</th>
                <th>Chefe da Delegação</th>
                <th>Campus</th>
            </tr>
            <tr>
                <td><?= $delegacao['Delegacao']['nome']; ?></td>
                <td><?= $delegacao['Servidor']['nome']; ?></td>
                <td><?= $delegacao['Campus']['nome'] . " em " . $delegacao['Campus']['cidade'] . "/" . $delegacao['Campus']['estado']; ?></td>
            </tr>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Gerar Crachá Individual</h4>
      </div>
      <div class="modal-body">
        <form action='<?= $this->Html->url("/crachas/gerarCrachaAtleta"); ?>' method="post">
            <fieldset>
                <label>Atleta:</label><br>
                <select name="atleta_modal" id="atleta_modal" class="form-control">
                    <option value selected> Selecione um Atleta </option>
                    <?php
                        foreach ($listAtletasDistinct as $keyModal => $modModal) {
                            echo "<option value='".$modModal['ModalidadesAtleta']['atleta_id']."'> ".$modModal['Atleta']['nome']. "</option>";
                        }
                    ?>
                </select>
                <input type="hidden" name="delegacao_id_url" value="<?= ($this->request->params['pass'][0]); ?>">
                <br>
                <button id="submitModal" type="submit" class="btn btn-primary"> Gerar </button>
            </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h3> Modalidades </h3>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td colspan="6  ">Modalidades x Atletas</td>
                    <td width="200">
                        <a href="<?= $this->Html->url(array('controller' => 'crachas', 'action' => 'gerarCrachaAtletaLote', $this->params['pass'][0])); ?>" class="btn btn-block btn-sm btn-success">Gerar Crachás em Lote</a>

                        <button class="btn btn-block btn-sm btn-primary" data-toggle="modal" data-target="#myModal">
                          Gerar Crachá Individual
                        </button>

                    </td>
                </tr>
                <tr>
                    <th>Numero</th>
                    <th>Aluno/Atleta</th>
                    <th>Data de Nascimento</th>
                    <th>Rg</th>
                    <th>N Matricula</th>
                    <th>Modalidade</th>
                    <th>Sub-Modalidade</th>
                </tr>
                <?php
                    foreach ($delegacao['ModalidadesAtleta'] as $key => $mod) {
                ?>
                <tr>
                    <td><?= ($key+1); ?> </td>
                    <td><?= $mod['Atleta']['nome']; ?> </td>
                    <td><?= $mod['Atleta']['data_nascimento']; ?> </td>
                    <td><?= $mod['Atleta']['rg']; ?> </td>
                    <td><?= $mod['Atleta']['matricula']; ?> </td>
                    <td><?= /*$mod['Modalidade']['name'];*/ $modalidades_raiz[$key]['Modalidade']['name']; ?> </td>
                    <td><?= $mod['Modalidade']['name'] == $modalidades_raiz[$key]['Modalidade']['name'] ? "<span style='color:#e74c3c'>Sem Sub-Modalidade</span>" : $mod['Modalidade']['name'] ?> </td>
                </tr>
                <?php } ?> 
            </table>
        </div>
    </div>
</div>
<?php
    //echo "<pre>";
    //print_r($delegacao);
    
?>