<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[2]).children('a').css({'color':'#e74c3c','background':"#eee"});

    $(document).ready(function(){
        $("#buscas").click(function(){
            $("#buscas-panel").slideToggle('fast');
        });

        $("#sub-menu").click(function(){
            $("#sub-menu-panel").slideToggle('fast');
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Atletas</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-success">
  <div id="sub-menu" class="panel-heading">Ações do Módulo <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="sub-menu-panel" style="display:block;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $this->Html->url(array('action' => 'cadastrarAtleta')); ?>">Cadastrar Atletas</a>
            |
            <a href="<?= $this->Html->url(array('action' => 'relacionarAtletaModalidade')); ?>">Cadastrar Atletas em Delegação</a>
            |
            <a href="<?= $this->Html->url(array('action' => 'relatorioAtletasExcel')); ?>">Relatório de Atletas (exportar .xls)</a>
        </div>
    </div>
  </div>
</div>

<div class="panel panel-info">
  <div id="buscas" class="panel-heading">Buscas <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="buscas-panel" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            Buscar Atleta por <b>Nome</b>
        </div>
        <div class="col-md-6">
            Buscar Atleta por <b>CPF</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_nome" id="termobusca_nome">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="termobusca_cpf" id="termobusca_cpf">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Buscar Atleta por <b>Câmpus</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_campus" id="termobusca_campus">
        </div>
        <div class="col-md-6">
            <input value="Buscar em filtros" type="submit" class="btn btn-primary btn-block">
        </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<hr class="divider">
<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th width="50"> <!-- imagem --> </th>
                <th>Nome do Atleta</th>
                <th>Campus</th>
                <th width='100'></th>
                <th width='100'></th>
                <th width='100'></th>
            </tr>
            <?php foreach ($atletas as $key => $atleta):
            ?>
            <tr>
                <td>
                    <?php $foto = empty($atleta['Atleta']['imagem']) ? "no-user.jpg" : "atletas/".$atleta['Atleta']['imagem']; ?>
                    <img src="<?= $this->Html->url('/img/' .  $foto); ?>" width="50" height="50" alt="foto_atleta">
                </td>
                <td><?= !$atleta['Atleta']['active'] ? '<span class="badge" style="color:#fff"> Inativo </span> &nbsp; <span style="color:#d1d1d1">'.$atleta['Atleta']['nome']. '</span>' : $atleta['Atleta']['nome'] ; ?></td>
                <td><?= $atleta['Campus']['nome']; ?></td>
                
                <td> <a <?= !$atleta['Atleta']['active'] ? 'disabled' : ''; ?> href='<?= $this->Html->url(array("action" => "editarAtleta", $atleta["Atleta"]["id"])); ?>' class="btn btn-block btn-sm btn-primary"> Alterar </a> </td>
                <td> <a <?= !$atleta['Atleta']['active'] ? 'disabled' : ''; ?> href='<?= $this->Html->url(array("action" => "visualizarAtleta", $atleta["Atleta"]["id"])); ?>' class="btn btn-block btn-sm btn-success"> Visualizar </a> </td>
                <td>
                    <?php echo $this->Form->postLink("Deletar", array('controller' => 'delegacoes', 'action' => 'deletarAtleta', $atleta['Atleta']['id']), array('class' => 'btn btn-block btn-sm btn-danger'), "Tem certeza?") ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>