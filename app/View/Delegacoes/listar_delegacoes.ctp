<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});

    $(document).ready(function(){
        $("#buscas").click(function(){
            $("#buscas-panel").slideToggle('fast');
        });

        $("#sub-menu").click(function(){
            $("#sub-menu-panel").slideToggle('fast');
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Delegações</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-success">
  <div id="sub-menu" class="panel-heading">Ações do Módulo <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="sub-menu-panel" style="display:block;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $this->Html->url(array('action' => 'cadastrarDelegacao')); ?>">Cadastrar Delegações</a>
            |
            <a href="<?= $this->Html->url(array('action' => 'relacionarAtletaModalidade')); ?>">Cadastrar Atletas em Delegação</a>
            |
            <a href="<?= $this->Html->url(array('action' => 'cadastrarAtleta')); ?>">Cadastrar Atleta</a>
            |
            <a href="" data-target="#myModal" data-toggle="modal">Relatório Atletas x Modalidades em Delegação (em .xls)</a>
        </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Relatório Atletas x Modalidades da Delegação</h4>
      </div>
      <div class="modal-body">
        <form action='<?= $this->Html->url("/delegacoes/relatorioAMDDownload"); ?>' method="post">
            <fieldset>
                <label>Delegação:</label><br>
                <select name="delegacao_modal" id="delegacao_modal" class="form-control">
                    <option value selected> Selecione uma Delegação </option>
                    <?php
                        foreach ($delegacoes as $keyModal => $modModal) {
                            echo "<option value='".$modModal['Delegacao']['id']."'> ".$modModal['Delegacao']['nome']. "</option>";
                        }
                    ?>
                </select>
                <br>
                <button id="submitModal" type="submit" class="btn btn-primary"> Gerar Relatório </button>
            </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="panel panel-info">
  <div id="buscas" class="panel-heading">Buscas <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="buscas-panel" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            Buscar Delegação por <b>Nome da Delegação</b>
        </div>
        <div class="col-md-6">
            Buscar Delegação por <b>Chefe da Delegação</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_nome" id="termobusca_nome">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="termobusca_chefedelegacao" id="termobusca_chefedelegacao">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Buscar Delegação por <b>Câmpus</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_campus" id="termobusca_campus">
        </div>
        <div class="col-md-6">
            <input value="Buscar em filtros" type="submit" class="btn btn-primary btn-block">
        </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<hr class="divider">
<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th>Nome da Delegação</th>
                <th>Chefe da Delegação</th>
                <th>Campus</th>
                <th width='100'></th>
                <th width='100'></th>
                <th width='100'></th>
            </tr>
            <?php foreach ($delegacoes as $key => $delegacao): ?>
            <tr>
                <td><?= !$delegacao['Delegacao']['active'] ? '<span class="badge" style="color:#fff"> Inativa </span> &nbsp; <span style="color:#d1d1d1">'.$delegacao['Delegacao']['nome']. '</span>' : $delegacao['Delegacao']['nome'] ; ?></td>
                <td><?= empty($delegacao['Servidor']['nome']) ? "<span style='color:#c0392b'> Sem Chefe </span>" : $delegacao['Servidor']['nome']; ?></td>
                <td><?= $delegacao['Campus']['nome']; ?></td>
                <td> <a <?= !$delegacao['Delegacao']['active'] ? 'disabled' : ''; ?> href='<?= $this->Html->url(array("action" => "editarDelegacao", $delegacao["Delegacao"]["id"])); ?>' class="btn btn-block btn-sm btn-primary"> Alterar </a> </td>
                <td> <a <?= !$delegacao['Delegacao']['active'] ? 'disabled' : ''; ?> href='<?= $this->Html->url(array("action" => "visualizarDelegacao", $delegacao["Delegacao"]["id"])); ?>' class="btn btn-block btn-sm btn-success"> Visualizar </a> </td>
                <td><?php echo $this->Form->postLink("Deletar", array('controller' => 'delegacoes', 'action' => 'deletarDelegacao', $delegacao['Delegacao']['id']), array('class' => 'btn btn-block btn-sm btn-danger'), "Tem certeza?") ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>