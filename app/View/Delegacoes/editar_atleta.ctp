
<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[2]).children('a').css({'color':'#e74c3c','background':"#eee"});
    jQuery(function($){
        $("#AtletaDataExpedicao").mask("99/99/9999");
        $("#AtletaNascimentoForm").mask("99/99/9999");
        $('#campu_id option').each(function(){
            if($(this).val() == <?= $this->request->data['Atleta']['campus'] ?>) {
                $(this).attr('selected', true);
            }
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar Atleta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    	<?php
    		$form = $this->Form;
    		echo $form->create('Atleta',array(
                'type' => 'post',
                'enctype' => 'multipart/form-data',
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'class' => 'form-control'
                )
            ));
            echo $form->input('id', array(
                'type' => 'hidden'
            ));
            echo $form->input('nome', array(
                'label' => 'Nome', 
                'id' => 'nome', 
                'required' => true
            ));
            echo $form->input('campu_id', array(
                'label' => 'Campus', 
                'id' => 'campu_id', 
                'options' => $this->request->data['Atleta']['campu_id']
            ));
            echo $form->input('sexo', array(
                'label' => 'Sexo', 
                'id' => 'sexo', 
                'options' => array('M' => 'Masculino', 'F' => 'Feminino')
            ));
            echo $form->input('nascimento_form', array(
                'value' => date('d/m/Y', strtotime($this->request->data['Atleta']['data_nascimento'])),
                'label' => 'Nascimento', 
                'required' => true
            ));
            ?>
            <div class="form-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      Clique para cadastrar dados adicionais
                    </a>
                  </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse out">
                  <div class="panel-body">
                    <?php
                        echo '<label class="checkbox">';
                        echo $form->checkbox('jif');
                        echo 'JIF </label>';
                        echo $form->input('telefone');
                        echo $form->input('banco');
                        echo $form->input('banco_op', array('label' => 'Banco - Operação'));
                        echo $form->input('banco_ag', array('label' => 'Banco - Agência'));
                        echo $form->input('banco_conta', array('label' => 'Banco - Conta'));
                        echo $form->input('tamanho_camisa', array('id' => 'camisa', 'options' => array(
                            '' => 'Selecione o tamanho',
                            'P' => 'P',
                            'M' => 'M',
                            'G' => 'G',
                            'GG' => 'GG',
                        )));
                        echo $form->input('tamanho_short', array('id' => 'short', 'options' => array(
                            '' => 'Selecione o tamanho',
                            'P' => 'P',
                            'M' => 'M',
                            'G' => 'G',
                            'GG' => 'GG',
                        )));
                        echo $form->input('cpf');
                        echo $form->input('rg');
                        echo $form->input('rg_orgexped', array('label' => 'Orgão Expedidor'));
                        echo $form->input('data_expedicao', array(
                            'label' => 'Data Expedição',
                            'value' => date('d/m/Y', strtotime($this->request->data['Atleta']['rg_exped'])),
                        ));
                        echo $form->input('email');
                        echo $form->input('tipagem_sangue', array('id' => 'tipagem', 'label' => 'Tipo Sanguíneo', 'options' => array(
                            '' => 'Selecione o Tipo Sanguíneo',
                            'A+' => 'A+',
                            'B+' => 'B+',
                            'AB+' => 'AB+',
                            'O-' => 'O-',
                            'A-' => 'A-',
                            'B-' => 'B-',
                            'AB-' => 'AB-',
                            'O+' => 'O+'
                        )));
                        echo $form->input('altura', array('label' => 'Altura (cm)'));
                        echo $form->input('massa', array('label' => 'Massa (kg)'));
                        echo $form->input('reacao_alergica', array('label' => 'Alergia'));
                        echo $form->input('medicamentos_uso', array('label' => 'Medicamentos'));
                        echo $form->input('doenca_cronica', array('label' => 'Doenças Crônicas'));
                        echo $form->input('curso_ifms', array('label' => 'Curso'));
                        echo $form->input('semestre_ingresso', array('label' => 'Semestre de Ingresso'));
                        echo $form->input('turno', array('id' => 'turno', 'options' => array(
                            'Matutino' => 'Matutino',
                            'Vespertino' => 'Vespertino',
                            'Noturno' => 'Noturno',
                            'Integral' => 'Integral'
                        )));
                        echo $form->input('matricula', array('label' => 'Número de Matrícula'));
                        echo $form->input('endereco', array('label' => 'Endereço Residencial'));
                        echo $form->input('cidade', array('label' => 'Município'));
                        echo $form->input('responsavel', array('label' => 'Nome do Pai/Mãe/Responsável'));
                        echo $form->input('email_responsavel', array('label' => 'Email do Responsável'));
                        echo $form->input('telefone_responsavel', array('label' => 'Telefone do Responsável'));
                        echo '<label class="checkbox">';
                        echo $form->checkbox('federado');
                        echo 'Atleta Federado </label>';
                        echo $form->input('periodo_treinamento', array('label' => 'Período em Treinamento (20xx - 20xx)'));
                        echo $form->input('obs', array('label' => 'Observações'));
                        echo $this->Form->input('imagem', array('type' => 'file', 'label' => 'Foto (opcional)', 'id' => 'username', 'class' => 'form-control'));
                    ?>
                  </div>
                </div>
            </div>
            <?php
            echo $this->Form->submit('Cadastrar', array('name' => 'submit', 'id' => 'btn_submitCadastro', 'class' => 'btn btn-primary'));
            echo $form->end();
            ?>
    </div>
</div>