<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[2]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Visualizar Atleta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <h2> <?= $atleta['Campus']['nome'] . " / " . $atleta['Campus']['estado']; ?> </h2>
        <table class="table table-bordered">
            <tr>
                <td colspan="2">Informações sobre o Atleta</td>
                <td> <a class="btn btn-sm btn-primary" href='<?= $this->Html->url(array('action' => 'editarAtleta', $atleta['Atleta']['id'])); ?>'><span class='glyphicon glyphicon-pencil'></span> Editar</a></td>
            </tr>
            <tr>
                <th style='width:33%'> Nome </th>
                <th style='width:33%'> Telefone </th>
                <th style='width:34%'> Sexo </th>
            </tr>
            <tr>
                <td> <?= $atleta['Atleta']['nome']; ?> </td>
                <td> <?= $atleta['Atleta']['telefone']; ?> </td>
                <td> <?= $atleta['Atleta']['sexo'] == 'M' ? 'Masculino' : 'Feminino'; ?> </td>
            </tr>
            <tr>
                
                <th> Email </th>
                <th> CPF </th>
                <th> RG </th>
            </tr>
            <tr>
                <td> <?= $atleta['Atleta']['email']; ?> </td>
                <td> <?= $atleta['Atleta']['cpf']; ?> </td>
                <td> <?= $atleta['Atleta']['rg'] ?> </td>
            </tr>
            <tr>
                <th> Informações Conta  </th>
                <th> Uniforme </th>
                <th> Tipo Sanguíneo </th>
            </tr>
            <tr>
                <td >
                    <b>Banco:</b> <?= $atleta['Atleta']['banco']; ?>,
                    <b> Operação: </b> <?= $atleta['Atleta']['banco_op']; ?>,
                    <b> Agência: </b> <?= $atleta['Atleta']['banco_ag']; ?>,
                    <b> Conta: </b> <?= $atleta['Atleta']['banco_conta']; ?>,
                </td>
                <td> 
                    <b>Tamanho Camisa: </b> <?= $atleta['Atleta']['tamanho_camisa']; ?>,
                    <b>Tamanho Short: </b> <?= $atleta['Atleta']['tamanho_short']; ?>
                </td>
                <td> <?= $atleta['Atleta']['tipagem_sangue']; ?> </td>
            </tr>
            <tr>
                <th> Medidas  </th>
                <th> Reação Alérgica </th>
                <th> Medicamentos </th>
            </tr>
            <tr>
                <td >
                    <b>Altura:</b> <?= $atleta['Atleta']['altura']; ?> cm, 
                    <b> Massa: </b> <?= $atleta['Atleta']['massa']; ?> kg
                </td>
                <td> <?= $atleta['Atleta']['reacao_alergica']; ?> </td>
                <td> <?= $atleta['Atleta']['medicamentos_uso']; ?> </td>
            </tr>
            <tr>
                <th> Doenças Crônicas  </th>
                <th> Curso </th>
                <th> Semestre Ingresso </th>
            </tr>
            <tr>
                <td > <?= $atleta['Atleta']['doenca_cronica']; ?> </td>
                <td> <?= $atleta['Atleta']['curso_ifms']; ?>, <b>Turno </b> <?= $atleta['Atleta']['turno']; ?> </td>
                <td> <?= $atleta['Atleta']['semestre_ingresso']; ?> </td>
            </tr>
            <tr>
                <th> Número Matrícula </th>
                <th colspan="2"> Endereço Residencial </th>
            </tr>
            <tr>
                <td > <?= $atleta['Atleta']['matricula']; ?> </td>
                <td> <?= $atleta['Atleta']['endereco']; ?>, <?= $atleta['Atleta']['cidade']; ?> </td>
            </tr>
            <tr>
                <th> Responsável </th>
                <th> Federado </th>
                <th> Período de Treinameto </th>
            </tr>
            <tr>
                <td > 
                    <?= $atleta['Atleta']['responsavel']; ?>,  
                    <b>Email: </b> <?= $atleta['Atleta']['email_responsavel']; ?>,  
                    <b>Telefone: </b> <?= $atleta['Atleta']['telefone_responsavel']; ?>,  
                </td>
                <td> <?= ($atleta['Atleta']['federado']) ? 'SIM': 'NÃO' ?> </td>
                <td> <?= $atleta['Atleta']['periodo_treinamento']; ?> </td>
            </tr>
            <tr>
                <th colspan="3"> Observações </th>
            </tr>
            <tr>
                <td colspan="3"> <?= $atleta['Atleta']['obs']; ?> </td>
            </tr>
        </table>
    </div>
</div>