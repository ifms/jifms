<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});

    var atletas = [];

    $(document).ready(function(){
        $("#btn_submitCadastro").css('display', 'none');
        
        $("#delegacao_id").change(function(){
            $("#atletas").slideDown(700);
        }); 

        $(".atleta_item").click(function(){
            $("#btn_submitCadastro").css('display', 'block');

            var item_id = $(this).attr('atleta-id');
            
            if(atletas.indexOf(item_id) == "-1"){
                $(this).css('background', '#2ecc71');
                atletas.push(item_id);

                var atletas_string = "";
                for(var i=0; i < atletas.length; i++){
                    if(i == (atletas.length-1)){
                        atletas_string += atletas[i];
                    }else{
                        atletas_string += atletas[i] + "@";    
                    }
                }
                $("#hidden_atletas").val(atletas_string);
            }
            else{
                $(this).css('background', '#95a5a6');
                var index = atletas.indexOf(item_id);
                if (index > -1) {
                    atletas.splice(index, 1);
                }

                var atletas_string = "";
                for(var i=0; i < atletas.length; i++){
                    if(i == (atletas.length-1)){
                        atletas_string += atletas[i];
                    }else{
                        atletas_string += atletas[i] + "@";    
                    }
                }
                $("#hidden_atletas").val(atletas_string);
            }
        }); 
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Relacionar Modalidades do Atleta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php
            $form = $this->Form;
            echo $form->create('ModalidadesAtleta',array(
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'class' => 'form-control'
                )
            ));
            echo $form->input('modalidade_id', array(
                'label' => 'Selecione a Modalidade', 
                'id' => 'modalidade_id', 
                'options' => $this->request->data['ModalidadesAtleta']['modalidade_id'], 
                'empty' => 'Selecione uma Modalidade para relacionar os atletas'
            ));

            echo $form->input('delegacao_id', array(
                'label' => 'Selecione a delegação do Atleta', 
                'id' => 'delegacao_id', 
                'options' => $this->request->data['ModalidadesAtleta']['delegacao_id'], 
                'empty' => 'Selecione uma Delegação'
            ));

            echo "<div id='atletas' style='display:none; border:1px solid #d1d1d1; width:100%; height:auto; border-radius:5px; margin-top:10px;'>";
            echo "<div style='padding:10px;'> Selecione os atletas desta modalidade </div>";
            echo "<div style='padding:10px; position:fixed; bottom:0; background:#d1d1d1; width:100%; z-index:999; margin-left:-31px;' >
                <div class='col-md-7'>
                <input type='text' class='form-control' id='pesquisa-interna'>
                </div>
                <div class='col-md-4'>
                <button id='submit-pesquisa' class='btn btn-primary'> Pesquisar atleta </button> 
                </div>
            </div>";
                foreach($this->request->data['ModalidadesAtleta']['atleta_id'] as $key => $atleta){
                    echo "<div id='".$key."' atleta-id='".$key."' class='atleta_item' style='float:left; margin: 10px; width:200px; height:140px; background:#95a5a6; color:#fff; padding:10px; text-align:center; border-radius: 5px; cursor:pointer;'> <div style='font-size:3.0em;'>" . ucfirst($atleta[0]) . "</div><div class='nome-atleta'>" . strtoupper($atleta) . "</div></div>";  
                }
            echo "<div class='clearfix'></div>";
            echo "</div>";
            echo "<input type='hidden' name='atletas' id='hidden_atletas'>";
            echo "<br>";
            echo $form->submit('Cadastrar', array(
                'name' => 'submit', 
                'id' => 'btn_submitCadastro', 
                'class' => 'btn btn-primary'
            ));
            echo $form->end();
            ?>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#submit-pesquisa").click(function(e) {
            e.preventDefault();

            var termo_busca = $("#pesquisa-interna").val().toUpperCase();
            var elemento = $('.nome-atleta:contains("'+termo_busca+'")')[0];
            var offset = $(elemento).parents(".atleta_item").offset();
            $("html, body").animate({
                scrollTop: offset.top
            });
            /*
            $(".nome-atleta").each(function(){
                if(termo_busca == $(this).html()){
                    var id_element = $(this).attr("id");
                    var offset = $(this).parents(".atleta_item").offset();
                    $("html, body").animate({
                        scrollTop: offset.top
                    });
                }
            });*/
        });
    });

</script>