<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Visualizar Servidor</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <tr>
                <td colspan="2">Informações sobre o Servidor</td>
                <td> <a class="btn btn-sm btn-primary" href='<?= $this->Html->url(array('action' => 'editarServidor', $servidor['Servidor']['id'])); ?>'><span class='glyphicon glyphicon-pencil'></span> Editar</a></td>
            </tr>
            <tr>
                <th>Nome Servidor</th>
                <th>Usuário do Sistema</th>
                <th>Campus</th>
            </tr>
            <tr>
                <td><?= $servidor['Servidor']['nome']; ?></td>
                <td><?= empty($servidor['Servidor']['user_id']) ? "<span style='color:#e74c3c'>Sem Associação</span>" :  $servidor['User']['username']; ?></td>
                <td><?= $servidor['Campus']['nome'] . " em " . $servidor['Campus']['cidade'] . "/" . $servidor['Campus']['estado']; ?></td>
            </tr>
            <tr>
                <th>JIF</th>
                <th>Função</th>
                <th>Sexo</th>
            </tr>
            <tr>
                <td><?= $servidor['Servidor']['jif'] == 0 ? 'Não' : 'Sim'; ?></td>
                <td><?= $servidor['Servidor']['funcao']; ?></td>
                <td><?= $servidor['Servidor']['sexo'] == 'M' ? 'Masculino' : 'Feminino'; ?></td>
            </tr>
            <tr>
                <th>Data Nascimento</th>
                <th>Telefone</th>
                <th>Siape</th>
            </tr>
            <tr>
                <td><?= date('d/m/Y', strtotime($servidor['Servidor']['nascimento'])); ?></td>
                <td><?= $servidor['Servidor']['telefone']; ?></td>
                <td><?= empty($servidor['Servidor']['siape']) ? "<span style='color:#e74c3c'>Não Informado</span>" :  $servidor['Servidor']['siape']; ?></td>
            </tr>
            <tr>
                <th>RG</th>
                <th>CPF</th>
                <th>Email</th>
            </tr>
            <tr>
                <td><?= $servidor['Servidor']['rg'] . ', Org Exp ' . $servidor['Servidor']['rg_expedidor']; ?></td>
                <td><?= $servidor['Servidor']['cpf']; ?></td>
                <td><?= $servidor['Servidor']['email']; ?></td>
            </tr>

        </table>
    </div>
</div>