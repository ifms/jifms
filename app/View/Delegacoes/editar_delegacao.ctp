<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar Delegação</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    	<?php 
        $form = $this->Form;
        echo $form->create('Delegacao',array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control'
            ),
            'url' => array('controller' => 'delegacoes', 'action' => 'editarDelegacao', $this->request->data['Delegacao']['id']),
            'type' => 'post'
        ));
        echo $form->input('nome', array(
            'label' => 'Informe o nome da delegação', 
            'id' => 'nome'
        ));
        echo $form->input('id', array(
            'type' => 'hidden',
            'value' => $this->request->data['Delegacao']['id']
        ));
        ?>
        <label> Campus: </label>
        <select name='data[Delegacao][campu_id]' id='campu_id' class="form-control">
          <?php foreach($campu_id_opcoes as $key => $campu_op): 
          ?>
          <option value="<?= $key ?>" <?= $this->request->data['Delegacao']['campu_id'] == $key ? "selected" : "" ?> > <?= $campu_op; ?> </option>
          <?php endforeach; ?>
        </select>
        <br>
        <label> Chefe da delegação: </label>
        <select name='data[Delegacao][servidor_id]' id='servidor_id' class="form-control">
          <?php foreach($servidor_id_opcoes as $key => $servidor_op): 
          ?>
          <option value="<?= $key ?>" <?= $this->request->data['Delegacao']['servidor_id'] == $key ? "selected" : "" ?> > <?= $servidor_op; ?> </option>
          <?php endforeach; ?>
        </select>
        <br>
        <?php
        echo $form->submit('Editar', array('name' => 'submit', 
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary'
        ));
        echo $form->end();
        ?>	
    </div>
</div>