<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[3]).children('a').css({'color':'#e74c3c','background':"#eee"});

    $(document).ready(function(){
        $("#make-individual").click(function() {
            $("#individual").show("fast");
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Gerar Crachá do Atleta</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    	   <div class="alert alert-info"> O processo de gerar crachás pode ser feita em lote (todos) e individualmente. </div>

           <div class="row">
               <div class="col-md-6">
                   <button id="make-individual" class="btn btn-primary">Gerar Crachá Individual</button>
               </div>
               <div class="col-md-6">
                    <form method="post">
                        <input type="hidden" name='request_type' value="lote">
                        <button onclick="javascript:$(this).attr('disabled', 'disabled'); $(this).parent().submit()" 
                        type="submit" class="btn btn-primary">Gerar Crachás em lote</button>
                    </form>
               </div>
            </div>

            <div id="individual" class="row" style="display:none">
            <br>
                <div class="col-md-12">
                    <form method="post">
                        <fieldset>
                            <input type="hidden" name='request_type' value="individual">
                            <label>Selecione o atleta para gerar o crachá</label>
                            <select name="atletas">
                                <option value selected> Selecione um Atleta </option>
                                <?php
                                    foreach($this->request->data['atleta_id'] as $key => $atleta){
                                        echo "<option value='".$key."'> ". $atleta ." </option>";
                                    }
                                ?>
                            </select>
                            <br>
                            <input class="btn btn-primary" value='Gerar' type="submit">
                        </fieldset>
                    </form>
                </div>
            </div>
    </div>
</div>