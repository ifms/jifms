<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[0]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cadastrar Delegação</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    	<?php 
        $form = $this->Form;
        echo $form->create('Delegacao',array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control'
            )
        ));
        echo $form->input('nome', array(
            'label' => 'Informe o nome da delegação', 
            'id' => 'nome'
        ));
        echo $form->input('campu_id', array(
            'label' => 'Selecione o campus', 
            'id' => 'campu_id', 
            'options' => $this->request->data['Delegacao']['campu_id']
        ));
        echo $form->input('servidor_id', array(
            'label' => 'Selecione um chefe para a delegação', 
            'id' => 'servidor_id', 
            'options' => $this->request->data['Delegacao']['servidor_id'], 
            'empty' => 'Selecione um servidor (opcional)'
        ));
        ?>
        <div class="alert alert-warning">
            <b># Atenção #</b> o campo `chefe de delegação pode ser atribuído posteriormente.`
        </div>
        <?php
        echo $form->submit('Cadastrar', array('name' => 'submit', 
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary'
        ));
        echo $form->end();
        ?>	
    </div>
</div>