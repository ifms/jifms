<script>
    $("#inicioPainel").css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Painel Administrativo JIFMS</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Painel v1.0</div>
            <div class="panel-body">
                <p> Utilize o menu para gerenciar o sistema. </p>
                <p> Abaixo estão descritos os recursos do <b>Sistema</b> </p>
                <br>
                <div class="col-md-4">
                    <span class='glyphicon glyphicon-plus'></span> Cadastros Gerais
                    <ul>
                        <li><a href='<?= $this->Html->url(array('controller' => 'usuarios', 'action' => 'cadastrarUsuario')); ?>'> Usuários do Sistema </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'cadastrarDelegacao')); ?>'> Delegações </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'cadastrarServidor')); ?>'> Servidores </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'cadastrarAtleta')); ?>'> Atletas </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'relacionarAtletaModalidade')); ?>'> Atletas em Delegações </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'modalidades', 'action' => 'cadastrarModalidade')); ?>'> Modalidades </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'campus', 'action' => 'cadastrarCampus')); ?>'> Campus </a></li>
                        <li> Confrontos <span class="badge">Breve!</span> </li>
                        <li> Resultados <span class="badge">Breve!</span> </li>
                        <li> Sumulas <span class="badge">Breve!</span> </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <span class='glyphicon glyphicon-random'></span> Gerações de Dados
                    <ul>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarServidores')); ?>'> Gerar Crachás Servidores (Lote) </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarServidores')); ?>'> Gerar Crachás Servidores (Individual) </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarDelegacoes')); ?>'> Gerar Crachás Atletas (Lote) </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarDelegacoes')); ?>'> Gerar Crachás Atletas (Individual) </a></li>
                    </ul>
                    * Obs: Para gerar crachás para <b>Atletas</b> deve-se primeiramente clicar em visualizar delegação e então gerar os crachás pro delegação.
                </div>
                <div class="col-md-4">
                    <span class='glyphicon glyphicon-list-alt'></span> Relatórios
                    <ul>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarDelegacoes')); ?>'> Relatório de Atletas x Modalidades em Delegação </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarServidores')); ?>'> Relatório dos Servidores </a></li>
                        <li><a href='<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarAtletas')); ?>'> Relatório dos Atletas </a></li>
                        <li> Relatório de Confrontos <span class="badge">Breve!</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>