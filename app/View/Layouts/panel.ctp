<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Painel JIFMS</title>
        <?php
        //echo $this->Html->meta('icon');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('font-awesome/css/font-awesome');
        echo $this->Html->css('plugins/morris/morris-0.4.3.min');
        echo $this->Html->css('plugins/timeline/timeline');
        echo $this->Html->css('sb-admin');
        echo $this->Html->css('jifms');

        echo $this->Html->script('jquery-1.10.2');
        echo $this->Html->script('jquery-ui.min');
        echo $this->Html->script('jquery.form');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('plugins/metisMenu/jquery.metisMenu');
        echo $this->Html->script('plugins/morris/raphael-2.1.0.min');
        echo $this->Html->script('plugins/morris/morris');
        echo $this->Html->script('sb-admin');
        echo $this->Html->script('ckeditor/ckeditor');
        echo $this->Html->script('mask.min');
        
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <script type="text/javascript">
            var sistema;
            sistema = sistema || (function () {
                var div = $('<div class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h1>Carregando...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div></div></div></div></div></div>');
                return {
                    showLoading: function() {
                        $(div).modal();
                    },
                    hideLoading: function () {
                        $(div).modal('hide');
                    },

                };
            })();
        </script>
    </head>
    <body>
        <div id="wrapper">

            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img style="float:left; margin:6px 0px 0px 6px;" src="<?= $this->Html->url('/img/logoif.png'); ?>" width='200'> 
                    <?= $this->Html->link("Sistema JIFMS", array('controller' => 'panel', 'action' => 'index'), array('class' => 'navbar-brand')); ?>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

            </nav>
            <!-- /.navbar-static-top -->

            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li> <a>  Olá <b><?= AuthComponent::user('fullname'); ?></b> </a> </li>
                        <li>
                            <a id="inicioPainel" href="<?= $this->Html->url(array('controller' => 'panel', 'action' => 'index')); ?>"><i class="fa fa-bar-chart-o fa-fw"></i> Inicio Painel</a>
                        </li>
                        <li>
                            <a id="gerenciaDeUsuarios" href="#"><i class="fa fa-sitemap fa-fw"></i> Ger&ecirc;ncia de Usu&aacute;rios <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'cadastrarUsuario')); ?>">Cadastrar Usuário </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'gerenciarAcesso')); ?>">Gerenciar Acesso</a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'all')); ?>">Visualizar Usuários</a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'alterarSenha')); ?>"> Alterar Senha </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a id="gerenciaDeDelegacoes" href="#"><i class="fa fa-sitemap fa-fw"></i> Gerência de Delegações <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarDelegacoes')); ?>"> Listar Delegações </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarServidores')); ?>"> Listar Servidores </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'listarAtletas')); ?>"> Listar Atletas </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'modalidades', 'action' => 'listarModalidades')); ?>"> Listar Modalidades </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a id="gerenciaDeConfrontos" href="#"><i class="fa fa-sitemap fa-fw"></i> Gerência de Confrontos <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'confrontos', 'action' => 'listarConfrontos')); ?>"> Listar Confronto </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'confrontos', 'action' => 'cadastrarConfronto')); ?>"> Cadastrar Confronto </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a id="gerenciaDeCampus" href="#"><i class="fa fa-sitemap fa-fw"></i> Gerência de Campus <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'campus', 'action' => 'cadastrarCampus')); ?>"> Cadastrar Campus </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Html->url(array('controller' => 'campus', 'action' => 'listarCampus')); ?>"> Listar Campus </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                        
                    </ul>
                    <!-- /#side-menu -->
                </div>
                <!-- /.sidebar-collapse -->
            </nav>
            <!-- /.navbar-static-side -->

            <div id="page-wrapper">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->fetch('content'); ?>
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
    </body>
</html>
