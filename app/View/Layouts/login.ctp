<?php ?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Painel JIFMS</title>
        <?php
        //echo $this->Html->meta('icon');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('font-awesome/css/font-awesome');
        echo $this->Html->css('sb-admin');
        
        echo $this->Html->script('jquery-1.10.2');
        echo $this->Html->script('jquery.form');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('modal');
        
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
    </head>
    <body>
         <?php echo $this->Session->flash(); ?>
         <?php echo $this->fetch('content'); ?>
    </body>
</html>

<?php 
    /* Exibe os CodigosSQL */
    //echo $this->element('sql_dump'); 
?>
