<script>
    $("#gerenciaDeCampus").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeCampus + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Campus</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th>Nome Campus</th>
                <th>Cidade/Estado</th>
                <th width="100"></th>
                <th width="100"></th>
            </tr>
            <?php foreach ($campus_list as $key => $campu): ?>
            <tr>
                <td><?= $campu['Campus']['nome']; ?></td>
                <td><?= $campu['Campus']['cidade'] . ' - ' . $campu['Campus']['estado']; ?></td>
                <td> <a href='<?= $this->Html->url(array("action" => "editarCampus", $campu["Campus"]["id"])); ?>' class="btn btn-block btn-sm btn-primary"> Alterar </a> </td>
                <td><?php echo $this->Form->postLink("Deletar", array('controller' => 'campus', 'action' => 'deletarCampus', $campu['Campus']['id']), array('class' => 'btn btn-block btn-sm btn-danger'), "Tem certeza?") ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>