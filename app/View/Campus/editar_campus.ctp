<script>
    $("#gerenciaDeCampus").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeCampus + ul").children('li')[1]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Editar Campus</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <?php 
        echo $this->Form->create('Campus', 
            array(
                'type' => 'post',
                'url' => array(
                    'controller' => 'campus',
                    'action' => 'editarCampus', $this->request->data['Campus']['id']
                ),
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'class' => 'form-control'
                )
            )
        ); 
        echo $this->Form->input('id', array(
            'type' => 'hidden'
        ));
        echo $this->Form->input('nome', array(
            'label' => 'Informe o nome do campus', 
            'id' => 'nome'
        ));
        echo $this->Form->input('cidade', array(
            'label' => 'Informe a cidade em que o campus se localiza ', 
            'id' => 'cidade'
        ));
        echo $this->Form->input('estado', array(
            'label' => 'Informe o estado em que o campus se localiza ', 
            'id' => 'estado'
        ));
        echo $this->Form->submit('Editar', array(
            'name' => 'submit', 
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary'
        ));
        echo $this->Form->end();
        ?>
    </div>
</div>