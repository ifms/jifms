<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[3]).children('a').css({'color':'#e74c3c','background':"#eee"});
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Cadastrar Modalidade</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
    <?php 
        $form = $this->Form;
        echo $form->create('Modalidade', array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control'
            )
        )); 
        echo $form->input('name', array(
            'label' => 'Informe o nome da Modalidade', 
            'id' => 'nome'
        ));

        echo $form->input('parent_id', array(
            'label' => 'Selecione a modalidade pai', 
            'id' => 'parent_id', 
            'options' => $this->request->data['Modalidade']['parent_id'],
            'empty' => 'Selecione a Categoria (Opcional).'
        ));

        echo $form->input('sexo', array(
            'label' => 'Sexo', 
            'options' => array(
                '' => 'Misto',
                'M' => 'Masculino',
                'F' => 'Feminino'
            )
        ));

        echo '<label class="checkbox">';
        echo $form->checkbox('tipo', array(
            'value' => 'I',
            'name' => 'data[Modalidade][tipo][]',
            "hiddenField" => false
        ));
        echo 'Individual </label>';

        echo '<label class="checkbox">';
        echo $form->checkbox('tipo', array(
            'value' => 'D',
            'name' => 'data[Modalidade][tipo][]',
            "hiddenField" => false
        ));
        echo 'Em Dupla </label>';

        echo '<label class="checkbox">';
        echo $form->checkbox('tipo', array(
            'value' => 'C',
            'name' => 'data[Modalidade][tipo][]',
            "hiddenField" => false
        ));
        echo 'Coletiva (Em Equipe) </label>';

        echo $form->submit('Cadastrar', array(
            'id' => 'btn_submitCadastro', 
            'class' => 'btn btn-primary'
        ));
        echo $form->end();
        ?>	
    </div>
</div>