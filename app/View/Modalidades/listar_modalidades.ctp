<script>
    $("#gerenciaDeDelegacoes").parent('li').toggleClass('active').children('ul').collapse('toggle');
    $($("#gerenciaDeDelegacoes + ul").children('li')[3]).children('a').css({'color':'#e74c3c','background':"#eee"});

    $(document).ready(function(){
        $("#buscas").click(function(){
            $("#buscas-panel").slideToggle('fast');
        });

        $("#sub-menu").click(function(){
            $("#sub-menu-panel").slideToggle('fast');
        });
    });
</script>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Listar Modalidades</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-success">
  <div id="sub-menu" class="panel-heading">Ações do Módulo <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="sub-menu-panel" style="display:block;">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= $this->Html->url(array('action' => 'cadastrarModalidade')); ?>">Cadastrar Modalidades</a>
            |
            <a href="<?= $this->Html->url(array('controller' => 'delegacoes', 'action' => 'relacionarAtletaModalidade')); ?>">Cadastrar Modalidades em Delegação</a>
        </div>
    </div>
  </div>
</div>

<div class="panel panel-info">
  <div id="buscas" class="panel-heading">Buscas <span style="float:right; margin-top:4px;" class='glyphicon glyphicon-chevron-down'></span></div>
  <div class="panel-body" id="buscas-panel" style="display:none;">
    <div class="row">
        <div class="col-md-6">
            Buscar Modalidade por <b>Nome</b>
        </div>
        <div class="col-md-6">
            Buscar Modalidade por <b>Tipo</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_nome" id="termobusca_nome">
        </div>
        <div class="col-md-6">
            <input class="form-control" name="termobusca_tipo" id="termobusca_tipo">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            Buscar Modalidade por <b>Sexo</b>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input class="form-control" name="termobusca_sexo" id="termobusca_sexo">
        </div>
        <div class="col-md-6">
            <input value="Buscar em filtros" type="submit" class="btn btn-primary btn-block">
        </div>
    </div>
  </div>
</div>

<div class="clearfix"></div>
<hr class="divider">
<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th>Nome da Modalidade</th>
                <th width='100'></th>
                <th width='100'></th>
            </tr>
            <?php foreach ($modalidades as $key => $modalidade): ?>
            <tr>
                <td><?= $modalidade; ?></td>
                <td> <a href='<?= $this->Html->url(array("action" => "editarModalidade", $key)); ?>' class="btn btn-block btn-sm btn-primary"> Alterar </a> </td>
                <td><?php echo $this->Form->postLink("Inativar", array('controller' => 'modalidades', 'action' => 'deletarModalidade', $key), array('class' => 'btn btn-block btn-sm btn-danger'), "Tem certeza?") ?></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>