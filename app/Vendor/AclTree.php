<?php
	include_once APP . "Model/Role.php";

	class AclTree {

		public $objectAcl = null;
		public $aroColletion = array();
		public $acoColletion = array(
			1 => array( 
	            'id' => 1,
	            'alias' => 'AppController',
	            'descricao' => 'Toda a Aplicação'
	        ),
	        2 => array( 
	            'id' => 2,
	            'alias' => 'UsersController',
	            'parent_id' => 1,
	            'descricao' => 'Controle de Usuários'
	        ),
	        3 => array( 
	            'id' => 3,
	            'alias' => 'ModalidadesController',
	            'parent_id' => 1,
	            'descricao' => 'Controle de Modalidades'
	        ),
		    4 => array( 
	            'id' => 4,
	            'alias' => 'DelegacoesController',
	            'parent_id' => 1,
	            'descricao' => 'Controle de Delegações'
	        ),
	        5 => array( 
	            'id' => 5,
	            'alias' => 'CampusController',
	            'parent_id' => 1,
	            'descricao' => 'Gerenciar Campus'
	        ),
	        20 => array( 
	            'id' => 6,
	            'alias' => 'CrachasController',
	            'parent_id' => 1,
	            'descricao' => 'Gerenciar Crachás'
	        ),
	        26 => array( 
	            'id' => 7,
	            'alias' => 'ConfrontosController',
	            'parent_id' => 1,
	            'descricao' => 'Gerenciar Confrontos'
	        ),
		        6 => array( 
		            'alias' => 'UsersController#cadastrarUsuario ',
		            'parent_id' => 2,
		            'descricao' => 'Cadastrar Administrador'
		        ),
		        7 => array( 
		            'alias' => 'UsersController#gerenciarAcesso',
		            'parent_id' => 2,
		            'descricao' => 'Gerenciar Controle de Acesso'
		        ),
		        36 => array( 
		            'alias' => 'UsersController#all',
		            'parent_id' => 2,
		            'descricao' => 'Visualizar todos usuarios'
		        ),
		        37 => array( 
		            'alias' => 'UsersController#alterarSenha',
		            'parent_id' => 2,
		            'descricao' => 'Alterar Senha'
		        ),
		        8 => array( 
		            'alias' => 'DelegacoesController#cadastrarServidor',
		            'parent_id' => 4,
		            'descricao' => 'Cadastrar Servidor'
		        ),
		        9 => array( 
		            'alias' => 'DelegacoesController#cadastrarAtleta',
		            'parent_id' => 4,
		            'descricao' => 'Cadastrar Atleta'
		        ),
		        42 => array( 
		            'alias' => 'DelegacoesController#relatorioAtletasExcel',
		            'parent_id' => 4,
		            'descricao' => 'Relatorio de Atletas .xls'
		        ),
		        44 => array( 
		            'alias' => 'DelegacoesController#relatorioServidoresExcel',
		            'parent_id' => 4,
		            'descricao' => 'Relatorio de Servidores .xls'
		        ),
		        45 => array( 
		            'alias' => 'DelegacoesController#relatorioAMDDownload',
		            'parent_id' => 4,
		            'descricao' => 'Relatorio de Atletas x Modalidades em Delegacao (.xls)'
		        ),
		        10 => array( 
		            'alias' => 'DelegacoesController#cadastrarDelegacao',
		            'parent_id' => 4,
		            'descricao' => 'Cadastrar Delegação'
		        ),
		        31 => array( 
		            'alias' => 'DelegacoesController#editarDelegacao',
		            'parent_id' => 4,
		            'descricao' => 'Editar Delegação'
		        ),
		        32 => array( 
		            'alias' => 'DelegacoesController#deletarDelegacao',
		            'parent_id' => 4,
		            'descricao' => 'Inativar/Deletar Delegação'
		        ),
		        17 => array( 
		            'alias' => 'DelegacoesController#listarDelegacoes',
		            'parent_id' => 4,
		            'descricao' => 'Listar Delegações'
		        ),
		        18 => array( 
		            'alias' => 'DelegacoesController#visualizarDelegacao',
		            'parent_id' => 4,
		            'descricao' => 'Visualizar Delegação'
		        ),
		        19 => array( 
		            'alias' => 'DelegacoesController#listarServidores',
		            'parent_id' => 4,
		            'descricao' => 'Visualizar Servidores'
		        ),
		        33 => array( 
		            'alias' => 'DelegacoesController#editarServidor',
		            'parent_id' => 4,
		            'descricao' => 'Editar Servidor'
		        ),
		        34 => array( 
		            'alias' => 'DelegacoesController#visualizarServidor',
		            'parent_id' => 4,
		            'descricao' => 'Dados Gerais do Servidor'
		        ),
		        35 => array( 
		            'alias' => 'DelegacoesController#deletarServidor',
		            'parent_id' => 4,
		            'descricao' => 'Deletar Servidor'
		        ),
		        28 => array( 
		            'alias' => 'DelegacoesController#listarAtletas',
		            'parent_id' => 4,
		            'descricao' => 'Listar Atletas'
		        ),
		        38 => array( 
		            'alias' => 'DelegacoesController#editarAtleta',
		            'parent_id' => 4,
		            'descricao' => 'Editar Atletas'
		        ),
		        39 => array( 
		            'alias' => 'DelegacoesController#visualizarAtleta',
		            'parent_id' => 4,
		            'descricao' => 'Visualizar Atleta'
		        ),
		        40 => array( 
		            'alias' => 'DelegacoesController#deletarAtleta',
		            'parent_id' => 4,
		            'descricao' => 'Deletar Atleta (Inativar)'
		        ),
		        29 => array( 
		            'alias' => 'DelegacoesController#relacionarAtletaModalidade',
		            'parent_id' => 4,
		            'descricao' => 'Cadastrar atletas nas modalidades'
		        ),
				11 => array( 
				    'alias' => 'ModalidadesController#cadastrarModalidade',
				    'parent_id' => 3,
				    'descricao' => 'Cadastrar Modalidade'
				),
				30 => array( 
				    'alias' => 'ModalidadesController#listarModalidades',
				    'parent_id' => 3,
				    'descricao' => 'Listar Modalidade'
				),
				41 => array( 
				    'alias' => 'ModalidadesController#deletarModalidades',
				    'parent_id' => 3,
				    'descricao' => 'Inativar (Deletar) Modalidade'
				),
				12 => array( 
				    'alias' => 'CampusController#cadastrarCampus',
				    'parent_id' => 5,
				    'descricao' => 'Cadastrar Campus'
				),
				14 => array( 
				    'alias' => 'CampusController#listarCampus',
				    'parent_id' => 5,
				    'descricao' => 'Listar Campus'
				),
				15 => array( 
				    'alias' => 'CampusController#editarCampus',
				    'parent_id' => 5,
				    'descricao' => 'Editar Campus'
				),
				16 => array( 
				    'alias' => 'CampusController#deletarCampus',
				    'parent_id' => 5,
				    'descricao' => 'Deletar Campus'
				),
				13 => array( 
				    'alias' => 'PanelController',
				    'parent_id' => 1,
				    'descricao' => 'Visualizar painel'
				),
				21 => array( 
				    'alias' => 'CrachasController#gerarCrachaServidor',
				    'parent_id' => 6,
				    'descricao' => 'Gerar Crachás Servidores'
				),
				22 => array( 
				    'alias' => 'CrachasController#gerarCrachaAtleta',
				    'parent_id' => 6,
				    'descricao' => 'Gerar Crachás Atletas'
				),
				23 => array( 
				    'alias' => 'CrachasController#gerarCrachaConvidado',
				    'parent_id' => 6,
				    'descricao' => 'Gerar Crachás Convidado'
				),
				24 => array( 
				    'alias' => 'CrachasController#gerarCrachaServidorLote',
				    'parent_id' => 6,
				    'descricao' => 'Gerar Crachás Servidores Lote'
				),
				25 => array( 
				    'alias' => 'CrachasController#gerarCrachaAtletaLote',
				    'parent_id' => 6,
				    'descricao' => 'Gerar Crachás Atletas Lote'
				),
				27 => array( 
				    'alias' => 'ConfrontosController#cadastrarConfronto',
				    'parent_id' => 7,
				    'descricao' => 'Cadastrar Confronto'
				),
				43 => array( 
				    'alias' => 'ConfrontosController#listarConfrontos',
				    'parent_id' => 7,
				    'descricao' => 'Listar Confrontos'
				)
	    );

	    public function __construct($obj){
	    	$this->objectAcl = $obj;
	    	$this->loadRoles();
	    }  

	    protected function loadRoles(){
			$role = new Role();
			$papeis = $role->find('all');
			$groups = array();
			foreach ($papeis as $key => $value) {
				$groups[] = array(
					'id' => $value['Role']['id'],
					'alias' => $value['Role']['papel']
				);
			}
			$this->aroColletion = $groups;
		}

	    public function deleteTreeAclAco(){
	    	$this->objectAcl->Aco->deleteAll(array('id >' => '0'), true);
		}

		public function buildTreeAclAco(){
		    $aco = $this->objectAcl->Aco;
		    $groups = $this->acoColletion;

		    echo "<pre>";
		    foreach ($groups as $data) {
		        print_r($data);
		        $aco->create();
		        if($aco->save($data))
		        	echo "Saved <br><br>"; 
		        else 
		        	"Not Saved <br><br>";
		    }
		    echo "</pre>";
		}  

		public function deleteTreeAclAro(){
	    	$this->objectAcl->Aro->deleteAll(array('id >' => '0'), true);
		}

		public function buildTreeAclAro(){
		    $aro = $this->objectAcl->Aro;
		    $groups = $this->aroColletion;

		    echo "<pre>";
		    foreach ($groups as $data) {
		        print_r($data);
		        $aro->create();
		        if($aro->save($data)) 
		        	echo "Saved <br><br>"; 
		        else echo 
		        	"Not Saved <br><br>";
		    }
		    echo "</pre>";
		}  

		public function buildDefaultPermissions() {
			$this->objectAcl->allow('Administrador', 'AppController');
		    $this->objectAcl->deny('Gestor Local', 'AppController');
		    $this->objectAcl->deny('Professor', 'AppController');
		    $this->objectAcl->deny('Arbitragem', 'AppController');
		    $this->objectAcl->deny('Suporte', 'AppController');
		}

	}