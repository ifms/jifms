<?php

App::uses('AppController', 'Controller');
App::import('Helper', 'Form');

class ConfrontosController extends AppController {
    public $layout = "panel";

    public function listarConfrontos() {
        $this->loadModel('Modalidade');
        $modalidades = $this->Modalidade->find('all');
        foreach ($modalidades as $i => $modalidade) {
            $confrontos = $this->Confronto->find('all', array(
                'conditions' => array(
                    'modalidade_id' => $modalidade['Modalidade']['id'],
                ),
                'order' => 'numero ASC'
            ));
            foreach ($confrontos as $j => $conf) {
                $this->loadModel('Delegacao');
                $this->Delegacao->recursive = -1;
                $delegacaoA = $this->Delegacao->findById(array(
                    'Delegacao.id' => $conf['Confronto']['delegacao_a']
                ));
                $delegacaoB = $this->Delegacao->findById(array(
                    'Delegacao.id' => $conf['Confronto']['delegacao_b']
                ));
                //print_r($delegacaoA); exit();
                $confrontos[$j]['Confronto']['delegacao_a'] = $delegacaoA['Delegacao']['nome'];
                $confrontos[$j]['Confronto']['delegacao_b'] = $delegacaoB['Delegacao']['nome'];
            }
             $modalidades[$i]['Confrontos'] = $confrontos;
        }
        $this->set(compact('modalidades'));
    }

    public function construirConfrontos($modalidadeId) {
        $this->layout = 'ajax';
        if ($this->request->is('post')) {
            $this->loadModel('Delegacao');
            $delegacoes = $this->Delegacao->find('all',array(
                'contain' => array(
                    'ModalidadesAtleta' => array(
                        'conditions' => array('modalidade_id' => $modalidadeId),
                    )
                ),
            ));
            $cont = 0;
            $delegacoesComAtletas = array();
            foreach ($delegacoes as $i => $delegacao) {
                if(count($delegacao['ModalidadesAtleta']) > 0) {
                    $delegacoesComAtletas[] = $delegacao;
                    $cont++;
                }
            }
            //print_r($cont); exit();
            $revezamento = array();
            $revezamentoGrupos = array(
                'grupoA' => array(),
                'grupoB' => array()
            );
            if($cont == 2) {
                foreach ($delegacoesComAtletas as $i => $delegacao) {
                    $idA = array(
                        'id' => $delegacoesComAtletas[0]['Delegacao']['id'], 
                        'nome' => $delegacoesComAtletas[0]['Delegacao']['nome']
                    );
                    $idB = array(
                        'id' => $delegacoesComAtletas[1]['Delegacao']['id'], 
                        'nome' => $delegacoesComAtletas[1]['Delegacao']['nome']
                    );
                    $revezamento[0] = array($idA, $idB);
                    $revezamento[1] = array($idA, $idB);
                }
            } else if($cont < 5) {
                foreach ($delegacoesComAtletas as $i => $delegacao) {
                    if(count($delegacao['ModalidadesAtleta']) > 0) {
                        $idA = array(
                            'id' => $delegacao['Delegacao']['id'], 
                            'nome' => $delegacao['Delegacao']['nome']
                        );
                        foreach ($delegacoesComAtletas as $j => $delegacaoIn) {
                            if(count($delegacaoIn['ModalidadesAtleta']) > 0) {
                                $idB = array(
                                    'id' => $delegacaoIn['Delegacao']['id'], 
                                    'nome' => $delegacaoIn['Delegacao']['nome']
                                );
                                if(
                                    !(in_array(array($idA, $idB), $revezamento) ||
                                    in_array(array($idB, $idA), $revezamento) ) &&
                                    $idA != $idB
                                ) {
                                    $revezamento[] = array($idA, $idB);
                                }
                            }
                        }
                    }
                }
            } else {
                $countGrupoA = ceil($cont/2);
                $countGrupoB = $cont - $countGrupoA;
                foreach ($delegacoesComAtletas as $i => $delegacao) {
                    // print_r('Count: ' . count($delegacao['ModalidadesAtleta']));
                    // print_r('GrupoA: ' . $countGrupoA . '<br>');
                    if($i < $countGrupoA) {
                        $idA = array(
                            'id' => $delegacao['Delegacao']['id'], 
                            'nome' => $delegacao['Delegacao']['nome']
                        );
                        foreach ($delegacoesComAtletas as $j => $delegacaoIn) {
                            //print_r('Count: ' . count($delegacaoIn['ModalidadesAtleta']));
                            //print_r('GrupoA: ' . $countGrupoA . '<br>');
                            if($j < $countGrupoA) {
                                $idB = array(
                                    'id' => $delegacaoIn['Delegacao']['id'], 
                                    'nome' => $delegacaoIn['Delegacao']['nome']
                                );
                                if(
                                    !(in_array(array($idA, $idB), $revezamentoGrupos['grupoA']) ||
                                    in_array(array($idB, $idA), $revezamentoGrupos['grupoA']) ) &&
                                    $idA != $idB
                                ) {
                                    $revezamentoGrupos['grupoA'][] = array($idA, $idB);
                                }
                            }
                        }
                    }
                }
                if($countGrupoB == 2) {
                    $idA = array(
                        'id' => $delegacoesComAtletas[3]['Delegacao']['id'], 
                        'nome' => $delegacoesComAtletas[3]['Delegacao']['nome']
                    );
                    $idB = array(
                        'id' => $delegacoesComAtletas[4]['Delegacao']['id'], 
                        'nome' => $delegacoesComAtletas[4]['Delegacao']['nome']
                    );
                    $revezamentoGrupos['grupoB'][0] = array($idA, $idB);
                    $revezamentoGrupos['grupoB'][1] = array($idA, $idB);
                } else {
                    foreach ($delegacoesComAtletas as $i => $delegacao) {
                        if($i >= $countGrupoA) {
                            $idA = array(
                                'id' => $delegacao['Delegacao']['id'], 
                                'nome' => $delegacao['Delegacao']['nome']
                            );
                            foreach ($delegacoesComAtletas as $j => $delegacaoIn) {
                                if($j >= $countGrupoA) {
                                    $idB = array(
                                        'id' => $delegacaoIn['Delegacao']['id'], 
                                        'nome' => $delegacaoIn['Delegacao']['nome']
                                    );
                                    if(
                                        !(in_array(array($idA, $idB), $revezamentoGrupos['grupoB']) ||
                                        in_array(array($idB, $idA), $revezamentoGrupos['grupoB']) ) &&
                                        $idA != $idB
                                    ) {
                                        $revezamentoGrupos['grupoB'][] = array($idA, $idB);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->set(compact('modalidadeId'));
            $this->set(compact('delegacoes'));
            $this->set(compact('revezamento'));
            $this->set(compact('revezamentoGrupos'));
        }
    }

    protected function cadastrarData($data) {
        $d = explode('/', $data);
        if (count($d) == 3) {
            return $d[2] . '-' . $d[1] . '-' .  $d[0];
        }
        return null;
    }

    public function cadastrarConfronto() {
        if ($this->request->is('post')) {
            foreach ($this->request->data['Confronto'] as $i => $confronto) {
                if (!empty($confronto['data_form']) && !empty($confronto['hora_form'])) {
                    $data = $this->cadastrarData($confronto['data_form']) . ' ' . $confronto['hora_form'] . ':00';
                    $this->request->data['Confronto'][$i]['data'] = $data;
                }
            }
            if($this->Confronto->saveAll($this->request->data['Confronto'])) {
                $this->Session->setFlash(__('Confrontos cadastrados com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'cadastrarConfronto'));
            }
        }

        $this->loadModel('Modalidade');
        $modalidadesArray = $this->Modalidade->generateTreeList();

        foreach ($modalidadesArray as $i => $modalidade) {
            $models = $this->Modalidade->getPath($i);
            $path = '';
            foreach ($models as $model) {
                $tipo = "";
                if($model['Modalidade']['tipo'] == 'I') {
                    $tipo = ' (Individual)';
                } else if($model['Modalidade']['tipo'] == 'D') {
                    $tipo = ' (Dupla)';
                } else if($model['Modalidade']['tipo'] == 'C') {
                    $tipo = ' (Coletivo)';
                }

                $sexo = "";
                if($model['Modalidade']['sexo'] == 'F') {
                    $sexo = ' (Feminino)';
                } else if($model['Modalidade']['sexo'] == 'M') {
                    $sexo = ' (Masculino)';
                }

                $path .= $model['Modalidade']['name'] . $tipo . $sexo . ' > ';
            }
            $path = substr($path, 0, strlen($path) - 3);
            $modalidadesArray[$i] = $path;
        }
        $this->request->data['Modalidade']['parent_id'] = $modalidadesArray;
    }

}
?>