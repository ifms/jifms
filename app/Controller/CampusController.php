<?php

App::uses('AppController', 'Controller');

class CampusController extends AppController {
    public $layout = "panel";
    public $uses = "Campus";
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('');
    }

    public function cadastrarCampus(){
        if($this->request->is('post')){
            if($this->Campus->save($this->request->data))
                $this->Session->setFlash(__("Campus cadastrado com sucesso"), 'sucesso');
            else{
                $this->Session->setFlash(__("Erro ao cadastrar campus"), 'erro');
            }
            $this->redirect(array('action' => 'cadastrarCampus'));
        }
    }

    public function listarCampus(){
        $this->set('campus_list', $this->Campus->find('all'));
    }

    public function editarCampus($id){
        $campus = $this->Campus->findById($id);
        
        if($this->request->is("post")){
            if($this->Campus->save($this->request->data))
                $this->Session->setFlash(__("Alterado com sucesso!"), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao alterar."), 'erro');
            $this->redirect(array("action" => 'editarCampus', $campus['Campus']['id']));
        }

        $this->request->data = $campus;
    }

    public function deletarCampus(){}

}
?>