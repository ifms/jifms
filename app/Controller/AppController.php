<?php

App::uses('Controller', 'Controller');
App::uses('AclTree', 'Vendor');

class AppController extends Controller {

	public $components = array(
        'Session', 'Acl',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'panel', 'action' => 'index'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'loginError' => "<div class='alert alert-danger'>Usuário e/ou senha incorreto(s)</div>",
            'authError' => "<div class='alert alert-warning'>Você precisa estar <b>autenticado</b> para acessar esta página</div>",
            'authorize' => array('Controller')
        )
    );

    public function isAuthorized($user) {
        $this->loadModel('User');
        $user = $this->User->findById($user['id']);
        foreach ($user['Role'] as $role) {
            $controller = $this->request->params['controller'] . 'Controller';
            if($this->Acl->check($role['papel'], ucwords($controller))) {
                return true;
            }
            $action = $controller . '#' . $this->request->params['action'];
            if($this->Acl->check($role['papel'], ucwords($action))) {
                return true;
            }
        }
        $this->redirect(array('controller' => 'panel', 'action' => 'index'));
        return false;
    }

    public function updateAclTree(){
        $AclTree = new AclTree($this->Acl);
        $AclTree->deleteTreeAclAco();
        $AclTree->buildTreeAclAco();
        $AclTree->deleteTreeAclAro();
        $AclTree->buildTreeAclAro();
        $AclTree->buildDefaultPermissions();
        exit();
    }

}
