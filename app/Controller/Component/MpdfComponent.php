<?php
/**
 * Component for working with mPDF class.
 * mPDF has to be in the vendors directory.
 */

class MpdfComponent extends Component {
	
	protected $pdf;
	
	protected $_configuration = array(
		// mode: 'c' for core fonts only, 'utf8-s' for subset etc.
		'mode' => 'utf8-s',
		// page format: 'A0' - 'A10', if suffixed with '-L', force landscape
		'format' => 'A4',
		 // default font size in points (pt)
		'font_size' => 0,
		// font
		'font' => NULL,
		//mm
		'margin_left' => 15,
		'margin_right' => 15,
		'margin_top' => 16,
		'margin_bottom' => 16,
		'margin_header' => 9,
		'margin_footer' => 9
	);
	
	protected $_init = false;
	
	protected $_filename = NULL;
	
	protected $_output = 'I';

	public function init($configuration = array()) {
		error_reporting(0);
		App::import('Vendor', 'mpdf/mpdf');
		if (!class_exists('mPDF'))
			throw new CakeException('Vendor class not found!');

		$c = array();
		foreach ($this->_configuration as $key => $val)
			$c[$key] = array_key_exists($key, $configuration) ? $configuration[$key] : $val;
		// initialize
		$this->pdf = new mPDF($c['mode'], $c['format'], $c['font_size'], $c['font'], $c['margin_left'], $c['margin_right'], $c['margin_top'], $c['margin_bottom'], $c['margin_header'], $c['margin_footer']);
		$this->_init = true;
	}

	public function setFilename($filename) {
		$this->_filename = (string)$filename;
	}

	/**
	 * Set destination of the output 
	 */
	public function setOutput($output) {
		if (in_array($output, array('I', 'D', 'F', 'S')))
			$this->_output = $output;
	}

	public function writeAndOutput($html){

		$this->pdf->WriteHTML((string)$html);
		$this->pdf->Output($this->_filename, $this->_output);

		exit;
	}


	/**
	 * Passing method calls and variable setting to mPDF library.
	 */
	public function __set($name, $value) {
		$this->pdf->$name = $value;
	}

	public function __get($name) {
		return $this->pdf->$name;
	}

	public function __isset($name) {
		return isset($this->pdf->$name);
	}

	public function __unset($name) {
		unset($this->pdf->$name);
	}

	public function __call($name, $arguments) {
		call_user_func_array(array($this->pdf, $name), $arguments);
	}
}