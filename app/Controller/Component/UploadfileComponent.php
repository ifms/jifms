<?php

App::uses('Component', 'Controller');

class UploadfileComponent extends Component {
    
    public $errors;
    
    protected function getExtensao($nomeArquivo){
        $arrayNome = strtolower(end(explode(".", $nomeArquivo)));
        return $arrayNome;
    }

    public function upload(Array $arquivo, $name, Array $permitidos, $pasta){
        $extensao = $this->getExtensao($arquivo['name']);
        if(in_array($extensao, $permitidos)){
            $destination =  WWW_ROOT . "/files/" . $pasta . "/";
            if(move_uploaded_file($arquivo['tmp_name'], $destination . $name . '.' . $extensao))
                return $name . "." . $extensao;
            else
                return 0;
        }else
            return 1;
        
    }

}

?>
