<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::import('Vendor','PHPExcel/Classes/PHPExcel'); 

class DelegacoesController extends AppController {
    public $uses = "Delegacao";
    public $layout = "panel";
    public $components = array('RequestHandler', 'Imagem');

    public function cadastrarDelegacao() {
        if ($this->request->is('post')) {
            if ($this->Delegacao->save($this->request->data)) {
                $this->Session->setFlash(__('Delegação cadastrada com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'cadastrarDelegacao'));
            }
        }

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'Campus.order' => 'nome ASC'
        ));

        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->request->data['Delegacao']['campu_id'] = $arrCampus;

        $this->loadModel("Servidor");
        $User_list = $this->Servidor->find('all', array(
            'Servidor.order' => 'nome ASC'
        ));
        
        $arrUser = array();
        foreach ($User_list as $value) {
            $arrUser[$value['Servidor']['id']] = $value['Servidor']['nome'];
        }
        $this->request->data['Delegacao']['servidor_id'] = $arrUser;
    }

    public function editarDelegacao($id = null){
        if(!$id){
           $this->Session->setFlash(__("Delegação não encontrada."), 'erro');
            $this->redirect(array('action' => 'listarDelegacoes')); 
        }
        $delegacao = $this->Delegacao->findById($id);
        
        if($this->request->is('post')){
            $this->Delegacao->create();
            if($this->Delegacao->save($this->request->data))
                $this->Session->setFlash(__("Delegação Atualizada."), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao atualizar Delegação."), 'erro');
            $this->redirect(array('action' => 'listarDelegacoes')); 
        }

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'Campus.order' => 'nome ASC'
        ));
        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->set('campu_id_opcoes', $arrCampus);
        $this->loadModel("Servidor");
        $User_list = $this->Servidor->find('all', array(
            'Servidor.order' => 'nome ASC'
        ));
        $arrUser = array();
        foreach ($User_list as $value) {
            $arrUser[$value['Servidor']['id']] = $value['Servidor']['nome'];
        }
        $this->set('servidor_id_opcoes', $arrUser);

        $this->request->data = $delegacao;
    }

    public function deletarDelegacao($id){
        $this->autoRender = false;

        if($this->request->is('post')){
            $this->Delegacao->read(null, $id);
            $this->Delegacao->set('active', 0);
            if($this->Delegacao->save())
                $this->Session->setFlash(__("Delegação Inativada com sucesso."), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao inativar Delegação."), 'erro');
            $this->redirect(array('action' => 'listarDelegacoes')); 
        }
    }

    public function listarDelegacoes(){
        $this->set('delegacoes', $this->Delegacao->find('all'));
    }

    public function visualizarDelegacao($id = null){
        if(!$id){
            $this->Session->setFlash(__("Delegação não encontrada."), 'erro');
            $this->redirect(array('action' => 'listarDelegacoes'));
        }
        $this->Delegacao->recursive = 2;
        $delegacao = $this->Delegacao->findById($id);

        $arrModalidadesPai = array();
        $this->loadModel("Modalidade");
        foreach ($delegacao['ModalidadesAtleta'] as $key => $value) {
            $parents = $this->Modalidade->getPath($value['Modalidade']['id']);
            $arrModalidadesPai[] = $parents[0];
        }

        $this->loadModel('ModalidadesAtleta');
        $listAtletasDistinct = $this->ModalidadesAtleta->find('all', array(
            'fields' => array('DISTINCT ModalidadesAtleta.atleta_id', 'Atleta.nome'),
            'conditions' => array('ModalidadesAtleta.delegacao_id' => $id)
        ));

        $this->set('modalidades_raiz', $arrModalidadesPai);
        $this->set('listAtletasDistinct', $listAtletasDistinct);
        $this->set('delegacao', $delegacao);
    }

    protected function cadastrarData($data) {
        $d = explode('/', $data);
        if (count($d) == 3) {
            return $d[2] . '-' . $d[1] . '-' .  $d[0];
        }
        return null;
    }

    public function editarServidor($id = null){
        if(!$id){
           $this->Session->setFlash(__("Servidor não encontrado."), 'erro');
            $this->redirect(array('action' => 'listarServidores')); 
        }
        $this->loadModel("Servidor");
        $servidor = $this->Servidor->findById($id);

        if($this->request->is('post')){
            $nascimento = $this->cadastrarData($this->request->data['Servidor']['nascimento_form']);
            $this->request->data['Servidor']['nascimento'] = $nascimento;

            if (is_uploaded_file($this->request->data['Servidor']['imagem']['tmp_name'])) {
                if(empty($servidor['Servidor']['imagem']))
                    $name = "ServidorFoto_" . md5(uniqid(rand(), true)) . ".jpg";
                else
                    $name = $servidor['Servidor']['imagem'];
                
                $this->request->data['Servidor']['imagem'] = $this->Imagem->redimensionar($this->request->data['Servidor']['imagem'], $name, 500, 'servidores');
            } else {
                $this->request->data['Servidor']['imagem'] = $servidor['Servidor']['imagem'];
            }

            $this->Servidor->create();
            if($this->Servidor->save($this->request->data))
                $this->Session->setFlash(__("Servidor Atualizado."), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao atualizar Servidor."), 'erro');
            $this->redirect(array('action' => 'listarServidores')); 
        }

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'order' => 'nome ASC'
        ));
        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->set('campu_id_opcoes', $arrCampus);

        $this->loadModel('User');
        $usuariosDB = $this->User->find('all', array(
            'fields' => array('id', 'fullname', 'email')
        ));
        $usuarios = '<div class="form-group nome_email_server"><label for="ServidorSexo">Usuario</label>';
        $usuarios .= '<select name="data[Servidor][user_id]" class="form-control" id="ServidorUsuarioSelect">';
        $usuarios .= '<option value=""> Selecione o Usuário </option>';
        foreach ($usuariosDB as $usuario) {
            $usuarios .= 
                '<option email-usuario="' . $usuario["User"]["email"] . '" value="' . $usuario["User"]["id"] . '">' . 
                $usuario['User']['fullname'] . 
                '</option>';
        }
        $usuarios .= '</select></div>';
        $this->set(compact(('usuarios')));

        $this->request->data = $servidor;
    }

    public function visualizarServidor($id = null){
        if(!$id){
           $this->Session->setFlash(__("Servidor não encontrado."), 'erro');
            $this->redirect(array('action' => 'listarServidores')); 
        }
        $this->loadModel("Servidor");
        $servidor = $this->Servidor->findById($id);
        $this->set('servidor', $servidor);
    }

    public function deletarServidor($id){
        $this->autoRender = false;
        if($this->request->is('post')){
            $this->loadModel('Servidor');
            $servidor = $this->Servidor->findById($id);
            if (!$servidor) {
                $this->Session->setFlash(__('Servidor não foi encontrado!'), 'erro');
                return $this->redirect(array('action' => 'listarServidores'));
            }
            $chefeDelegacao = $this->Delegacao->find('all', array(
                'conditions' => array('servidor_id' => $id)
            ));
            foreach ($chefeDelegacao as $key => $value) {
                $this->Delegacao->create();
                $value['Delegacao']['servidor_id'] = '';
                $this->Delegacao->save($value);
            }
            if ($this->Servidor->delete($id))   
                $this->Session->setFlash(__('Servidor deletado'), 'sucesso'); 
            else
                $this->Session->setFlash(__('Servidor não deletado'), 'erro'); 
            return $this->redirect(array('action' => 'listarServidores'));
        }
    }

    /* Relatorio de Atletas x Modalidades x Delegacao para Download em XLS (Excel) */
    public function relatorioAMDDownload(){ 
        $this->autoRender = false;
        if($this->request->is('post')){
            $delegacao_modal = $this->request->data['delegacao_modal'];
        }

        $this->loadModel('ModalidadesAtleta');
        $Atletas_x_Modalidades = $this->ModalidadesAtleta->find('all', array(
            'conditions' => array('delegacao_id' => $delegacao_modal)
        ));

        $delegacao_data = $this->Delegacao->findById($delegacao_modal);

        $ModalidadesAtletaDistinct = array();
        $posicao = 0;
        for ($i=0; $i<count($Atletas_x_Modalidades); $i++) {
            $achou = false;
            for ($j=($i+1); $j<count($Atletas_x_Modalidades); $j++) {
                if($Atletas_x_Modalidades[$i]['ModalidadesAtleta']['atleta_id'] === $Atletas_x_Modalidades[$j]['ModalidadesAtleta']['atleta_id']){
                    $achou = true;
                }
            }   
            if(!$achou){
                $ModalidadesAtletaDistinct[$posicao] = $Atletas_x_Modalidades[$i]['ModalidadesAtleta']['atleta_id'];
                $posicao++;
            }
        }

        $ModalidadesDoAtleta = array();
        for ($i=0; $i<count($ModalidadesAtletaDistinct); $i++) {
            for ($j=0; $j<count($Atletas_x_Modalidades); $j++) {
                if($ModalidadesAtletaDistinct[$i] === $Atletas_x_Modalidades[$j]['ModalidadesAtleta']['atleta_id']){
                    $ModalidadesDoAtleta[$i][] = $Atletas_x_Modalidades[$j];
                }
            }   
        }

        $this->loadModel("Modalidade");
        foreach ($ModalidadesDoAtleta as $key => $value) {
            foreach ($value as $keyDeep => $valueDeep) {
                $parents = $this->Modalidade->getPath($valueDeep['ModalidadesAtleta']['modalidade_id']);
                $ModalidadesDoAtleta[$key][$keyDeep]['ModalidadesAtleta']['nome_modalidade'] = $parents[0]['Modalidade']['name'];
            }
        }

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("LSA&THFS - Systems")
                                     ->setLastModifiedBy("LSA&THFS - Systems")
                                     ->setTitle("Relatorio Atletas_x_Modalidades Sistema")
                                     ->setSubject("Relatorio Atletas_x_Modalidades Sistema")
                                     ->setDescription("Relatorio Atletas_x_Modalidades Sistema")
                                     ->setKeywords("Relatorio Atletas_x_Modalidades Sistema")
                                     ->setCategory("Relatorio Atletas_x_Modalidades Sistema");

        for($a=0; $a<count($ModalidadesAtletaDistinct); $a++){

            $ModalidadesIndivduais = array();
            for($m=0; $m<count($ModalidadesDoAtleta[$a]); $m++){
                if(!in_array($ModalidadesDoAtleta[$a][$m]['ModalidadesAtleta']['nome_modalidade'], $ModalidadesIndivduais))
                    $ModalidadesIndivduais[] = $ModalidadesDoAtleta[$a][$m]['ModalidadesAtleta']['nome_modalidade'];
            }
            $strModalidades = "";
            for($modIn=0; $modIn<count($ModalidadesIndivduais); $modIn++){
                $strModalidades .= " | " . $ModalidadesIndivduais[$modIn] . " | ";
            }

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['nome'])
                    ->setCellValue('B' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['sexo'])
                    ->setCellValue('C' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['telefone'])
                    ->setCellValue('D' . ($a+1), date('d/m/Y', strtotime($ModalidadesDoAtleta[$a][0]['Atleta']['data_nascimento'])))
                    ->setCellValue('E' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['rg'])
                    ->setCellValue('F' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['cpf'])
                    ->setCellValue('G' . ($a+1), $ModalidadesDoAtleta[$a][0]['Atleta']['email'])
                    ->setCellValue('H' . ($a+1), $strModalidades);
        }
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . (count($ModalidadesAtletaDistinct)+1), "Delegação: ".$delegacao_data['Delegacao']['nome']);

        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ListaAtletas_x_Modalidades_Delegacao_'.$delegacao_data['Delegacao']['nome'].'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function listarServidores(){
        $this->loadModel('Servidor');
        $this->set('servidores', $this->Servidor->find('all'));
    }

    public function relatorioServidoresExcel(){
        $this->autoRender = false;

        $this->loadModel('Servidor');
        $servidores = $this->Servidor->find('all');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("LSA&THFS - Systems")
                                     ->setLastModifiedBy("LSA&THFS - Systems")
                                     ->setTitle("Relatorio Servidores Sistema")
                                     ->setSubject("Relatorio Servidores Sistema")
                                     ->setDescription("Relatorio Servidores Sistema")
                                     ->setKeywords("Relatorio Servidores Sistema")
                                     ->setCategory("Relatorio Servidores Sistema");

        foreach($servidores as $key => $servidor):
                // Add some data
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . ($key+1), $servidor['Servidor']['nome'])
                    ->setCellValue('B' . ($key+1), $servidor['Campus']['nome'])
                    ->setCellValue('C' . ($key+1), $servidor['Servidor']['sexo'])
                    ->setCellValue('D' . ($key+1), date('d/m/Y', strtotime($servidor['Servidor']['nascimento'])))
                    ->setCellValue('E' . ($key+1), $servidor['Servidor']['rg'])
                    ->setCellValue('F' . ($key+1), $servidor['Servidor']['cpf'])
                    ->setCellValue('G' . ($key+1), $servidor['Servidor']['funcao'])
                    ->setCellValue('H' . ($key+1), $servidor['Servidor']['email']);
        endforeach;
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ListaServidores'.date('d_m_Y').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        //header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        //header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        //header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function cadastrarServidor() {
        if ($this->request->is('post')) {
            $this->loadModel('Servidor');
            $nascimento = $this->cadastrarData($this->request->data['Servidor']['nascimento_form']);
            $this->request->data['Servidor']['nascimento'] = $nascimento;
            if (is_uploaded_file($this->request->data['Servidor']['imagem']['tmp_name'])) {
                $name = "ServidorFoto_" . md5(uniqid(rand(), true)) . ".jpg";
                $this->request->data['Servidor']['imagem'] = $this->Imagem->redimensionar($this->request->data['Servidor']['imagem'], $name, 500, 'servidores');
            } else {
                $this->request->data['Servidor']['imagem'] = "";
            }
            if ($this->Servidor->save($this->request->data)) {
                $this->Session->setFlash(__('Servidor cadastrado com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'cadastrarServidor'));
            }
        }

        $this->loadModel('User');
        $usuariosDB = $this->User->find('all', array(
            'fields' => array('id', 'fullname', 'email')
        ));
        $usuarios = '<div class="form-group nome_email_server"><label for="ServidorSexo">Usuario</label>';
        $usuarios .= '<select name="data[Servidor][user_id]" class="form-control" id="ServidorUsuarioSelect">';
        $usuarios .= '<option value=""> Selecione o Usuário </option>';
        foreach ($usuariosDB as $usuario) {
            $usuarios .= 
                '<option email-usuario="' . $usuario["User"]["email"] . '" value="' . $usuario["User"]["id"] . '">' . 
                $usuario['User']['fullname'] . 
                '</option>';
        }
        $usuarios .= '</select></div>';
        $this->set(compact(('usuarios')));

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'order' => 'nome ASC'
        ));
        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->set(compact('arrCampus'));
    }

    public function listarAtletas(){
        $this->loadModel('Atleta');
        $this->set('atletas', $this->Atleta->find('all',array(
            //'conditions' => array('NOT' => array('Atleta.active' => 0)),
            'order' => array('Campus.id ASC', 'Atleta.nome ASC')
        )));
    }

    public function visualizarAtleta($id = null){
        if(!$id){
           $this->Session->setFlash(__("Atleta não encontrado."), 'erro');
            $this->redirect(array('action' => 'listarAtletas')); 
        }
        $this->loadModel("Atleta");
        $atleta = $this->Atleta->findById($id);
        $this->set('atleta', $atleta);
    }

    public function relatorioAtletasExcel(){
        $this->autoRender = false;

        $this->loadModel('Atleta');
        $atletas = $this->Atleta->find('all',array(
            //'conditions' => array('NOT' => array('Atleta.active' => 0)),
            'order' => array('Campus.id ASC', 'Atleta.nome ASC')
        ));

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("LSA&THFS - Systems")
                                     ->setLastModifiedBy("LSA&THFS - Systems")
                                     ->setTitle("Relatorio Atletas Sistema")
                                     ->setSubject("Relatorio Atletas Sistema")
                                     ->setDescription("Relatorio Atletas Sistema")
                                     ->setKeywords("Relatorio Atletas Sistema")
                                     ->setCategory("Relatorio Atletas Sistema");

        foreach($atletas as $key => $atleta):
                // Add some data
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . ($key+1), $atleta['Atleta']['nome'])
                    ->setCellValue('B' . ($key+1), $atleta['Campus']['nome'])
                    ->setCellValue('C' . ($key+1), $atleta['Atleta']['sexo'])
                    ->setCellValue('D' . ($key+1), date('d/m/Y', strtotime($atleta['Atleta']['data_nascimento'])))
                    ->setCellValue('E' . ($key+1), ($atleta['Atleta']['active'] == 1 ? "Ativo" : "Inativo"))
                    ->setCellValue('F' . ($key+1), $atleta['Atleta']['rg'])
                    ->setCellValue('G' . ($key+1), $atleta['Atleta']['cpf'])
                    ->setCellValue('H' . ($key+1), $atleta['Atleta']['email']);
        endforeach;
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ListaAtletas'.date('d_m_Y').'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        //header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        //header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        //header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function cadastrarAtleta() {
        if ($this->request->is('post')) {
            $this->loadModel("Atleta");
            $nascimento = $this->cadastrarData($this->request->data['Atleta']['nascimento_form']);
            $this->request->data['Atleta']['data_nascimento'] = $nascimento;
            $nascimento = $this->cadastrarData($this->request->data['Atleta']['data_expedicao']);
            $this->request->data['Atleta']['rg_exped'] = $nascimento;
            if (is_uploaded_file($this->request->data['Atleta']['imagem']['tmp_name'])) {
                $name = "AtletaFoto_" . md5(uniqid(rand(), true)) . ".jpg";
                $this->request->data['Atleta']['imagem'] = $this->Imagem->redimensionar($this->request->data['Atleta']['imagem'], $name, 500, 'atletas');
            } else {
                $this->request->data['Atleta']['imagem'] = "";
            }
            if ($this->Atleta->save($this->request->data)) {
                $this->Session->setFlash(__('Atleta cadastrado com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'cadastrarAtleta'));
            }
        }

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'order' => 'nome ASC'
        ));

        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->request->data['Atleta']['campu_id'] = $arrCampus;

    }

    public function deletarAtleta($id){
        $this->autoRender = false;

        if($this->request->is('post')){
            $this->loadModel('Atleta');

            $this->Atleta->read(null, $id);
            $this->Atleta->set('active', 0);
            if($this->Atleta->save())
                $this->Session->setFlash(__("Atleta Inativado com sucesso."), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao inativar atleta."), 'erro');
            $this->redirect(array('action' => 'listarAtletas')); 
        }
    }

    public function editarAtleta($id = null) {
        if(!$id) $this->redirect(array('action' => 'listarAtletas'));
        $this->loadModel("Atleta");
        $atleta = $this->Atleta->findById($id);

        if(!$atleta) $this->redirect(array('action' => 'listarAtletas'));

        if ($this->request->is('post')) {
            $nascimento = $this->cadastrarData($this->request->data['Atleta']['nascimento_form']);
            $this->request->data['Atleta']['data_nascimento'] = $nascimento;
            $nascimento = $this->cadastrarData($this->request->data['Atleta']['data_expedicao']);
            $this->request->data['Atleta']['rg_exped'] = $nascimento;
            
            if (is_uploaded_file($this->request->data['Atleta']['imagem']['tmp_name'])) {
                if(empty($atleta['Atleta']['imagem']))
                    $name = "AtletaFoto_" . md5(uniqid(rand(), true)) . ".jpg";
                else
                    $name = $atleta['Atleta']['imagem'];
                
                $this->request->data['Atleta']['imagem'] = $this->Imagem->redimensionar($this->request->data['Atleta']['imagem'], $name, 500, 'atletas');
            } else {
                $this->request->data['Atleta']['imagem'] = $atleta['Atleta']['imagem'];
            }

            if ($this->Atleta->save($this->request->data)) {
                $this->Session->setFlash(__('Atleta atualizado com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'editarAtleta', $id));
            } else {
                $this->Session->setFlash(__('Erro ao atualizar atleta.'), 'erro');
                $this->redirect(array('action' => 'editarAtleta', $id));
            }
        }

        $this->request->data = $atleta;
        $this->request->data['Atleta']['campus'] = $atleta['Atleta']['campu_id'];

        $this->loadModel("Campus");
        $campus_list = $this->Campus->find('all', array(
            'order' => 'nome ASC'
        ));

        $arrCampus = array();
        foreach ($campus_list as $value) {
            $arrCampus[$value['Campus']['id']] = $value['Campus']['nome'] . ' em ' . $value['Campus']['cidade'] . ' / ' . $value['Campus']['estado'];
        }
        $this->request->data['Atleta']['campu_id'] = $arrCampus;

    }


    public function relacionarAtletaModalidade(){
        if($this->request->is("post")){
            $this->loadModel("ModalidadesAtleta");
            $atletas = explode('@', $this->request->data['atletas']);
            $quantidadeAtletas = count($atletas);
            $errors = 0;
            $success = 0;
            foreach($atletas as $atleta){
                $findExistsAtleta = $this->ModalidadesAtleta->find('first', array(
                    'conditions' => array(
                         'ModalidadesAtleta.atleta_id' => $atleta,
                         'ModalidadesAtleta.modalidade_id' => $this->request->data['ModalidadesAtleta']['modalidade_id'],
                         'ModalidadesAtleta.delegacao_id' => $this->request->data['ModalidadesAtleta']['delegacao_id']
                    )
                ));
                
                $this->ModalidadesAtleta->create();
                $data = array('ModalidadesAtleta' => array(
                    'id' => (!empty($findExistsAtleta) ? $findExistsAtleta['ModalidadesAtleta']['id'] : ''),
                    'atleta_id' => $atleta,
                    'modalidade_id' => $this->request->data['ModalidadesAtleta']['modalidade_id'],
                    'delegacao_id'  => $this->request->data['ModalidadesAtleta']['delegacao_id']
                ));
                
                if($this->ModalidadesAtleta->save($data))
                    $success++;
                else
                    $errors++;
            }
            $this->Session->setFlash(__('De ' . $quantidadeAtletas . ' atletas, ' . $success . ' foram cadastrados com sucesso, e ' . $errors . ' tiveram erro ao serem cadastrados.'), 'sucesso');
            $this->redirect(array("action" => 'relacionarAtletaModalidade'));
        }

        $this->loadModel("Modalidade");
        $Modalidade_list = $this->Modalidade->generateTreeList();
        foreach ($Modalidade_list as $i => $modalidade) {
            $models = $this->Modalidade->getPath($i);
            $path = '';
            foreach ($models as $model) {
                $tipo = "";
                if($model['Modalidade']['tipo'] == 'I') {
                    $tipo = ' (Individual)';
                } else if($model['Modalidade']['tipo'] == 'D') {
                    $tipo = ' (Dupla)';
                } else if($model['Modalidade']['tipo'] == 'C') {
                    $tipo = ' (Coletivo)';
                }

                $sexo = "";
                if($model['Modalidade']['sexo'] == 'F') {
                    $sexo = ' (Feminino)';
                } else if($model['Modalidade']['sexo'] == 'M') {
                    $sexo = ' (Masculino)';
                }

                $path .= $model['Modalidade']['name'] . $tipo . $sexo . ' > ';
            }
            $path = substr($path, 0, strlen($path) - 3);
            $Modalidade_list[$i] = $path;
        }
        $this->request->data['ModalidadesAtleta']['modalidade_id'] = $Modalidade_list;


        $this->loadModel("Atleta");
        $Atleta_list = $this->Atleta->find('all', array(
            'conditions' => array('NOT' => array('Atleta.active' => 0)),
            'order' => 'Atleta.nome ASC'
        ));
        $arrAtleta = array();
        foreach ($Atleta_list as $value) {
            $arrAtleta[$value['Atleta']['id']] = $value['Atleta']['nome'] . ", " . $value['Campus']['nome'];
        }
        $this->request->data['ModalidadesAtleta']['atleta_id'] = $arrAtleta;


        $Delegacao_list = $this->Delegacao->find('all', array(
            'order' => 'Delegacao.nome ASC'
        ));
        $arrDelegacao = array();
        foreach ($Delegacao_list as $value) {
            $arrDelegacao[$value['Delegacao']['id']] = $value['Delegacao']['nome'] . ", " . $value['Campus']['nome'];
        }
        $this->request->data['ModalidadesAtleta']['delegacao_id'] = $arrDelegacao;
    }

    public function gerarCrachaAtleta(){

        $this->loadModel("Atleta");
        $Atleta_list = $this->Atleta->find('all', array(
            'order' => 'Atleta.nome ASC'
        ));
        $arrAtleta = array();
        foreach ($Atleta_list as $value) {
            $arrAtleta[$value['Atleta']['id']] = $value['Atleta']['nome'] . ", " . $value['Campus']['nome'];
        }
        $this->request->data['atleta_id'] = $arrAtleta;

    }   

}
?>