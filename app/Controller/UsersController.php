<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {
    public $layout = "panel";
    public $components = array('RequestHandler');
    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout', 'updateAclTree');
    }

    private function sendNewUserEmail(Array $data){
        try {
            CakeEmail::deliver(
                $data['User']['email'], 
                "Olá " . $data['User']['fullname'] . ", seja bem vindo(a) ao JIFMS.", 
                
                "<div style='font-size:16px; color:#333;'> Você foi adionado ao JIFMS </div><br>" .
                "Dados para acesso a sua conta: <br>" .
                "Login: " . $data['User']['username'] . "<br>" .
                "Senha: " . $data['User']['password'] . " <br>" .
                "<a href='" . Router::url(array('full' => true, 'controller' => 'users', 'action' => 'login'), true) . "'>Acessar Área Restrita JIFMS</a>",
                'ifms'
            );
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function all() {
        
        $this->User->recursive = 0;
        $this->paginate = array('conditions' => 
            array('User.active' => true),
            'limit' => 20,
        );
        $users = $this->paginate('User');
        $this->set('users', $users);
    }

    public function cadastrarUsuario() {
        $this->loadModel("Role");
        $roles = $this->Role->find('all', array(
            'fields' => array('Role.id', 'Role.papel')
        ));
        $options = array();
        foreach($roles as $key => $val){
            $options[$val['Role']['id']] = $val['Role']['papel']; 
        }

        if ($this->request->is('post')) {
            $this->User->create();

            $username = explode(' ', $this->request->data['User']['fullname']);
            $username = $username[0].".".end($username);
            $this->request->data['User']['username'] = strtolower($username);
            $this->request->data['User']['password'] = 123;

            if ($this->User->save($this->request->data)) {
                $this->loadModel('UsersRole');
                foreach($this->request->data['User']['perfil'] as $value){
                    $this->UsersRole->create();
                    $data = array('UsersRole' => array(
                        'user_id' => $this->User->id,
                        'role_id' => $value,
                    ));
                    $this->UsersRole->save($data);    
                }

                $this->sendNewUserEmail($this->request->data);

                $this->Session->setFlash(__('Usuario cadastrado com sucesso!'), 'sucesso');
                $this->redirect(array('action' => 'cadastrarUsuario'));
            }
        }

        $this->set('roles', $options);
    }

    public function login() {
        $this->layout = 'login';
        if ($this->request->is("post")) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            }
            if (!$this->Auth->login()) {
                $this->Session->setFlash(__('Erro: Login ou senha inválida'), 'erro');
            }
        }
    }

    public function logout() {
        $this->layout = 'login';
        $this->redirect($this->Auth->logout());
    }

    public function delete($id){
        $this->autoRender = true;
        if($this->User->delete((int)$id, $cascade = true))
            $this->Session->setFlash(__('Deletado com sucesso!'), 'sucesso');
        else
            $this->Session->setFlash(__('Erro ao deletar.'), 'erro');
        $this->redirect(array('action' => 'all'));
    }

    private function recursivelyGetArrayTree($papel, array $aco) {
        $retorno = array(
            'title' => $aco['Aco']['descricao'],
            'key' => $aco['Aco']['alias'],
            'expanded' => ($aco['Aco']['alias'] == 'AppController') ? true : false,
            'selected' => ($this->Acl->check($papel, $aco['Aco']['alias'])) ? true : false
        );
        if(!empty($aco['children'])) {
            foreach ($aco['children'] as $child) {
                $retorno['children'][] = $this->recursivelyGetArrayTree($papel, $child);
            }
        }
        return $retorno;
    }

    public function gerenciarAcesso() {
        if ($this->request->is("post")) {
            $d = $this->request->data;
            if(!empty($d['aroId']) && !empty($d['aroAlias'])) {
                $this->loadModel('ArosAco');
                $roles = $this->ArosAco->deleteAll(array('ArosAco.aro_id' => $d['aroId']), true);
                $this->Acl->deny($d['aroAlias'], 'AppController');
                if(isset($d['access'])) {
                    foreach ($d['access'] as $aco) {
                        $this->Acl->allow($d['aroAlias'], $aco);
                    }
                }
            }
        }
        $acoTree = $this->Acl->Aco->find('threaded'); 
        $this->loadModel('Role');
        $roles = $this->Role->find('all', array(
            'conditions' => array('papel NOT' => 'Administrador')
        ));
        foreach ($roles as $role) {
            $acoTree[0]['Role'] = $role['Role'];
            $acoArrayTree[$role['Role']['id']][] = $this->recursivelyGetArrayTree($role['Role']['papel'], $acoTree[0]);
        }
        //print_r($acoArrayTree); exit();
        $this->set('accessTree', $acoArrayTree);

        $this->loadModel('Aro');
        $roles = $this->Aro->find('all', array(
            'conditions' => array('alias NOT' => 'Administrador'),
            'fields' => array('Aro.id', 'Aro.alias')
        ));
        $this->set('roles', $roles);
    }

    public function alterarSenha() {
        if($this->request->is('post')){
            $user = $this->User->findById(AuthComponent::user('id'));
            if(AuthComponent::password($this->request->data['User']['senha_atual']) == $user['User']['password']) {
                if($this->request->data['User']['senha_nova'] == $this->request->data['User']['senha_repetir']) {
                    $user['User']['password'] = $this->request->data['User']['senha_nova'];
                    if($this->User->save($user))
                        $this->Session->setFlash(__('Senha alterada com sucesso!'), 'sucesso');
                    else
                        $this->Session->setFlash(__('Erro ao alterar senha'), 'erro');
                } else {
                    $this->Session->setFlash(__('As senhas informadas estão diferentes'), 'erro');
                }
            } else {
                $this->Session->setFlash(__('A senha informada não combina com a senha atual.'), 'erro');
            }
            $this->redirect(array('action' => 'alterarSenha'));
        }
    }

    public function updateAclTree(){
        parent::updateAclTree();
    }

}
?>