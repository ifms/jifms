<?php
	App::uses('AppController', 'Controller');
	App::uses('CakeEmail', 'Network/Email');

	class CrachasController extends AppController {
	    public $layout = "ajax";
	    public $components = array('Mpdf'); 

	    public function gerarCrachaServidor(){
	    	if($this->request->is("post")){
	    		$this->loadModel("Servidor");
	    		$servidor = $this->Servidor->findById($this->request->data['servidor_modal']);

	    		$foto = empty($servidor['Servidor']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/servidores/".$servidor['Servidor']['imagem'];
	    		$html = '
		        <table width="100%" cellspacing="5" cellpadding="0">
					<tr>
						<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<div style="text-align:center; color:#fff; font-size:27px; bottom:-130px; left:160px; width:350px; height:300px; position:absolute; display:block;">
									Organização
								</div>
								<div style="text-align:center; font-size:20px; top:270px; left:180px; width:350px; height:300px; position:absolute; display:block;">
									<img src="'.$foto.'" width="200">
									<br><br>
									'.$servidor['Servidor']['nome'].' <br>
									'.$servidor['Campus']['nome'].'
								</div>
								<img src="'.WWW_ROOT.'img/jifms_organizacao_frente.jpg">
							</td>
						<td align="center" style="border:0.1mm solid #d1d1d1;">
							<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
						</td>
					</tr>
				</table>
	        	'; 		
	        	$nomeServidor = str_replace(" ", "", $servidor['Servidor']['nome']);
	        	$this->Mpdf->init(array('format' => 'A4-L', 'margin_left' => '32', 'margin_right' => '32', 'margin_top' => '23', 'margin_bottom' => '23'));
			    $this->Mpdf->setFilename('CrachaIndividual_'.$nomeServidor.'.pdf'); 
		        $this->Mpdf->setOutput('I'); 
		        $this->Mpdf->writeAndOutput($html);
	    		exit;
	    	}
	    }

	    public function gerarCrachaServidorLote(){
	    	$this->loadModel('Servidor');
	    	$servidores = $this->Servidor->find('all');
	    	$html = "";
	    	for($key=0; $key<count($servidores); $key++){
	    		$foto1 = empty($servidores[$key]['Servidor']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/servidores/".$servidores[$key]['Servidor']['imagem'];
	    		$foto2 = empty($servidores[($key+1)]['Servidor']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/servidores/".$servidores[($key+1)]['Servidor']['imagem'];
	    		$ServidorNomeTd1 = isset($servidores[($key)]['Servidor']['nome']) ? $servidores[($key)]['Servidor']['nome'] : "";
	    		$ServidorNomeTd2 = isset($servidores[($key+1)]['Servidor']['nome']) ? $servidores[($key+1)]['Servidor']['nome'] : "";
	    		$ServidorCampusNomeTd1 = isset($servidores[($key)]['Campus']['nome']) ? $servidores[($key)]['Campus']['nome'] : "";
	    		$ServidorCampusNomeTd2 = isset($servidores[($key+1)]['Campus']['nome']) ? $servidores[($key+1)]['Campus']['nome'] : "";
		    	$html .= '
		    		<table width="100%" cellspacing="5" cellpadding="0">
		    			<tr>
		    				<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<div style="text-align:center; color:#fff; font-size:27px; bottom:-130px; left:160px; width:350px; height:300px; position:absolute; display:block;">
									Organização
								</div>
								<div style="text-align:center; font-size:20px; top:270px; left:180px; width:350px; height:300px; position:absolute; display:block;">
									<img src="'.$foto1.'" width="200">
									<br><br>
									'.$ServidorNomeTd1.' <br>
									'.$ServidorCampusNomeTd1.'
								</div>
								<img src="'.WWW_ROOT.'img/jifms_organizacao_frente.jpg">
							</td>
							<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<div style="text-align:center; color:#fff; font-size:27px; bottom:-130px; right:180px; width:350px; height:300px; position:absolute; display:block;">
									Organização
								</div>
								<div style="text-align:center; font-size:20px; top:270px; right:180px; width:350px; height:300px; position:absolute; display:block;">
									<img src="'.$foto2.'" width="200">
									<br><br>
									'.$ServidorNomeTd2.' <br>
									'.$ServidorCampusNomeTd2.'
								</div>
								<img src="'.WWW_ROOT.'img/jifms_organizacao_frente.jpg">
							</td>
		    			</tr>
		    		</table>
		    	';
		    	$key++;
		    	if($key != count($servidores)-1){ $html .= "<pagebreak />"; }
	    	}
	    	$html .= '
		    		<table width="100%" cellspacing="5" cellpadding="0">
		    			<tr>
		    				<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
							</td>
							<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
							</td>
		    			</tr>
		    		</table>
		    		<pagebreak />
		    	';

	    	$this->Mpdf->init(array('format' => 'A4-L', 'margin_left' => '32', 'margin_right' => '32', 'margin_top' => '23', 'margin_bottom' => '23'));
		    $this->Mpdf->setFilename('CrachasLote_Servidores.pdf'); 
	        $this->Mpdf->setOutput('I'); 
	        $this->Mpdf->writeAndOutput($html);
	    }

	    public function gerarCrachaAtleta(){
	    	if($this->request->is("post")){
	    		$this->loadModel("ModalidadesAtleta");
	    		$this->loadModel("Modalidade");
	    		$atleta_Modalidades = $this->ModalidadesAtleta->find('all', array(
	    			'conditions' => array('ModalidadesAtleta.atleta_id' => $this->request->data['atleta_modal'],
	    							'ModalidadesAtleta.delegacao_id' => $this->request->data['delegacao_id_url']
	    							)
	    		));

	    		$modalidadesString = "";
	    		$modalidadesArray = array();
	    		foreach($atleta_Modalidades as $key => $value){
	    			$parents = $this->Modalidade->getPath($atleta_Modalidades[$key]['Modalidade']['id']);
	    			if(!in_array($parents[0]['Modalidade']['name'], $modalidadesArray)){
	    				$modalidadesString .= " | " . $parents[0]['Modalidade']['name'] . " | ";
	    				$modalidadesArray[] = $parents[0]['Modalidade']['name'];
	    			}
	    		}
	    		$this->loadModel('Campus');
	    		$campus_info = $this->Campus->findById($atleta_Modalidades[0]['Atleta']['campu_id']);

	    		$foto = empty($atleta_Modalidades[0]['Atleta']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/atletas/".$atleta_Modalidades[0]['Atleta']['imagem'];
	    		$html = '
		        <table width="100%" cellspacing="5" cellpadding="0">
					<tr>
						<td align="center" style="border:0.1mm solid #d1d1d1; ">
							<div style="text-align:center; font-size:18px; bottom:-130px; left:180px; width:350px; height:300px; position:absolute; display:block;">
								'.$modalidadesString.'
							</div>
							<div style="text-align:center; font-size:20px; top:270px; left:180px; width:350px; height:300px; position:absolute; display:block;">
								<img src="'.$foto.'" width="200">
								<br><br>
								'.$atleta_Modalidades[0]['Atleta']['nome'].' <br>
								'.$campus_info['Campus']['nome'].'
							</div>
							<img src="'.WWW_ROOT.'img/jifms_cracha_frente.jpg">
						</td>
						<td align="center" style="border:0.1mm solid #d1d1d1;">
							<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
						</td>
					</tr>
				</table>
	        	'; 		
	        	$nomeAtleta = str_replace(" ", "", $atleta_Modalidades[0]['Atleta']['nome']);
	        	$this->Mpdf->init(array('format' => 'A4-L', 'margin_left' => '32', 'margin_right' => '32', 'margin_top' => '23', 'margin_bottom' => '23'));
			    $this->Mpdf->setFilename('CrachaIndividual_'.$nomeAtleta.'.pdf'); 
		        $this->Mpdf->setOutput('I'); 
		        $this->Mpdf->writeAndOutput($html);
	    		exit;
	    	}
	    }

	    public function gerarCrachaAtletaLote($id = null){
	    	if(!$id){
	            $this->Session->setFlash(__("Delegação não encontrada."), 'erro');
	            $this->redirect(array('action' => 'listarDelegacoes'));
	        }
	        /* Carregando todas as modalidades da delegacao ordenando por atleta */
	        $this->loadModel('ModalidadesAtleta');
	        $this->ModalidadesAtleta->recursive = 2;
	        $delegacao = $this->ModalidadesAtleta->find('all', array(
	        	'conditions' => array('ModalidadesAtleta.delegacao_id' => $id),
	        	'order' => array('ModalidadesAtleta.atleta_id')
	        ));

	        $ModalidadesAtletaDistinct = array();
	        $posicao = 0;
	        for ($i=0; $i<count($delegacao); $i++) {
	        	$achou = false;
	        	for ($j=($i+1); $j<count($delegacao); $j++) {
		        	if($delegacao[$i]['ModalidadesAtleta']['atleta_id'] === $delegacao[$j]['ModalidadesAtleta']['atleta_id']){
		        		$achou = true;
		        	}
		        }	
		        if(!$achou){
		        	$ModalidadesAtletaDistinct[$posicao] = $delegacao[$i]['ModalidadesAtleta']['atleta_id'];
		        	$posicao++;
		        }
	        }	

	        $ModalidadesDoAtleta = array();

	        for ($i=0; $i<count($ModalidadesAtletaDistinct); $i++) {
	        	for ($j=0; $j<count($delegacao); $j++) {
		        	if($ModalidadesAtletaDistinct[$i] === $delegacao[$j]['ModalidadesAtleta']['atleta_id']){
		        		$ModalidadesDoAtleta[$i][] = $delegacao[$j];
		        	}
		        }	
	        }

	        $this->loadModel("Modalidade");
	        foreach ($ModalidadesDoAtleta as $key => $value) {
	        	foreach ($value as $keyDeep => $valueDeep) {
	        		$parents = $this->Modalidade->getPath($valueDeep['ModalidadesAtleta']['modalidade_id']);
	        		$ModalidadesDoAtleta[$key][$keyDeep]['ModalidadesAtleta']['nome_modalidade'] = $parents[0]['Modalidade']['name'];
	        	}
	        }

	        $html = "";

	        for($a=0; $a<count($ModalidadesDoAtleta); $a++){
	        	
	        	if(isset($ModalidadesDoAtleta[$a])){
		        	$ModalidadesIndivduais1 = array();
		        	for($m=0; $m<count($ModalidadesDoAtleta[$a]); $m++){
		        		if(!in_array($ModalidadesDoAtleta[$a][$m]['ModalidadesAtleta']['nome_modalidade'], $ModalidadesIndivduais1))
		        			$ModalidadesIndivduais1[] = $ModalidadesDoAtleta[$a][$m]['ModalidadesAtleta']['nome_modalidade'];
		        	}
	        	}

	        	if(isset($ModalidadesDoAtleta[$a+1])){
		        	$ModalidadesIndivduais2 = array();
		        	for($m=0; $m<count($ModalidadesDoAtleta[$a+1]); $m++){
		        		if(!in_array($ModalidadesDoAtleta[$a+1][$m]['ModalidadesAtleta']['nome_modalidade'], $ModalidadesIndivduais2))
		        			$ModalidadesIndivduais2[] = $ModalidadesDoAtleta[$a+1][$m]['ModalidadesAtleta']['nome_modalidade'];
		        	}
	        	}

	        	$strModalidades1 = "";
	        	for($modIn=0; $modIn<count($ModalidadesIndivduais1); $modIn++){
	        		$strModalidades1 .= " | " . $ModalidadesIndivduais1[$modIn] . " | ";
	        	}

	        	$strModalidades2 = "";
	        	for($modIn=0; $modIn<count($ModalidadesIndivduais2); $modIn++){
	        		$strModalidades2 .= " | " . $ModalidadesIndivduais2[$modIn] . " | ";
	        	}

	        	
	        	$foto1 = empty($ModalidadesDoAtleta[$a][0]['Atleta']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/atletas/".$ModalidadesDoAtleta[$a][0]['Atleta']['imagem'];
	        	$foto2 = empty($ModalidadesDoAtleta[$a+1][0]['Atleta']['imagem']) ? WWW_ROOT."img/no-user.jpg" : WWW_ROOT."img/atletas/".$ModalidadesDoAtleta[$a+1][0]['Atleta']['imagem'];

	        	$strModalidadeTd1 = isset($strModalidades1) ? $strModalidades1 : "";
	        	$strModalidadeTd2 = isset($strModalidades2) ? $strModalidades2 : "";

				$AtletaNomeTd1 = isset($ModalidadesDoAtleta[$a][0]['Atleta']['nome'])? $ModalidadesDoAtleta[$a][0]['Atleta']['nome'] : "";
				$AtletaCampusNomeTd1 = isset($ModalidadesDoAtleta[$a][0]['Atleta']['Campus']['nome']) ? $ModalidadesDoAtleta[$a][0]['Atleta']['Campus']['nome'] : "";

				$AtletaNomeTd2 = isset($ModalidadesDoAtleta[$a+1][0]['Atleta']['nome']) ? $ModalidadesDoAtleta[$a+1][0]['Atleta']['nome'] : "";
				$AtletaCampusNomeTd2 = isset($ModalidadesDoAtleta[$a+1][0]['Atleta']['Campus']['nome']) ? $ModalidadesDoAtleta[$a+1][0]['Atleta']['Campus']['nome'] : "";

	        	$html .= '
		        <table width="100%" cellspacing="5" cellpadding="0">
					<tr>
						<td align="center" style="border:0.1mm solid #d1d1d1; ">
							<div style="text-align:center; font-size:18px; bottom:-130px; left:180px; width:350px; height:300px; position:absolute; display:block;">
								'.$strModalidadeTd1.'
							</div>
							<div style="text-align:center; font-size:20px; top:270px; left:180px; width:350px; height:300px; position:absolute; display:block;">
								<img src="'.$foto1.'" width="200">
								<br><br>
								'.$AtletaNomeTd1.' <br>
								'.$AtletaCampusNomeTd1.'
							</div>
							<img src="'.WWW_ROOT.'img/jifms_cracha_frente.jpg">
						</td>
						<td align="center" style="border:0.1mm solid #d1d1d1; ">
							<div style="text-align:center; font-size:18px; bottom:-130px; right:180px; width:350px; height:300px; position:absolute; display:block;">
								'.$strModalidadeTd2.'
							</div>
							<div style="text-align:center; font-size:20px; top:270px; right:180px; width:350px; height:300px; position:absolute; display:block;">
								<img src="'.$foto2.'" width="200">
								<br><br>
								'.$AtletaNomeTd2.' <br>
								'.$AtletaCampusNomeTd2.'
							</div>
							<img src="'.WWW_ROOT.'img/jifms_cracha_frente.jpg">
						</td>
					</tr>
				</table>
	        	';
	        	if($a != count($ModalidadesDoAtleta)-1){ $html .= "<pagebreak />"; }
	        	$a++;
	        }
	        $html .= '
		    		<table width="100%" cellspacing="5" cellpadding="0">
		    			<tr>
		    				<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
							</td>
							<td align="center" style="border:0.1mm solid #d1d1d1; ">
								<img src="'.WWW_ROOT.'img/jifms_cracha_verso.jpg">
							</td>
		    			</tr>
		    		</table>
		    		<pagebreak />
		    	';

	        $this->Mpdf->init(array('format' => 'A4-L', 'margin_left' => '32', 'margin_right' => '32', 'margin_top' => '23', 'margin_bottom' => '23'));
		    $this->Mpdf->setFilename('CrachasLote_Delegacao.pdf'); 
	        $this->Mpdf->setOutput('I'); 
	        $this->Mpdf->writeAndOutput($html);
	    }

	    public function gerarCrachaConvidado(){}

	}
?>