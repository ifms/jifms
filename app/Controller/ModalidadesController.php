<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class ModalidadesController extends AppController {
    public $layout = "panel";
    public $components = array('RequestHandler');

    public function listarModalidades(){
        $modalidadesArray = $this->Modalidade->generateTreeList();
        foreach ($modalidadesArray as $i => $modalidade) {
            $models = $this->Modalidade->getPath($i);
            $path = '';
            foreach ($models as $model) {
                $tipo = "";
                if($model['Modalidade']['tipo'] == 'I') {
                    $tipo = ' (Individual)';
                } else if($model['Modalidade']['tipo'] == 'D') {
                    $tipo = ' (Dupla)';
                } else if($model['Modalidade']['tipo'] == 'C') {
                    $tipo = ' (Coletivo)';
                }

                $sexo = "";
                if($model['Modalidade']['sexo'] == 'F') {
                    $sexo = ' (Feminino)';
                } else if($model['Modalidade']['sexo'] == 'M') {
                    $sexo = ' (Masculino)';
                }

                $active = "";
                if($model['Modalidade']['active'] == 1) {
                    $active = ' (Mod Ativa)';
                } else if($model['Modalidade']['active'] == 0) {
                    $active = ' (Mod Inativa)';
                }

                $path .= $model['Modalidade']['name'] . $tipo . $sexo . $active . ' > ';
            }
            $path = substr($path, 0, strlen($path) - 3);
            $modalidadesArray[$i] = $path;
        }
        $this->set('modalidades', $modalidadesArray);
    }

    public function deletarModalidade($id){
        $this->autoRender = false;

        if($this->request->is('post')){
            $this->Modalidade->read(null, $id);
            $this->Modalidade->set('active', 0);
            if($this->Modalidade->save())
                $this->Session->setFlash(__("Modalidade Inativada com sucesso."), 'sucesso');
            else
                $this->Session->setFlash(__("Erro ao inativar Modalidade."), 'erro');
            $this->redirect(array('action' => 'listarModalidades')); 
        }
    }

    public function cadastrarModalidade() {
        if ($this->request->is('post')) {
            $tiposSelecionados = empty($this->request->data['Modalidade']['tipo']) ? 0 : $this->request->data['Modalidade']['tipo'];
            if($tiposSelecionados != 0){
                foreach($tiposSelecionados as $tipoModalidade){
                    $this->Modalidade->create();
                    $this->request->data['Modalidade']['tipo'] = $tipoModalidade;
                    if ($this->Modalidade->save($this->request->data)) {
                        $this->Session->setFlash(__('Modalidade cadastrada com sucesso!'), 'sucesso');
                    }
                }
            }else{
                $this->Modalidade->create();
                if ($this->Modalidade->save($this->request->data)) {
                    $this->Session->setFlash(__('Modalidade cadastrada com sucesso!'), 'sucesso');
                }
            }

            $this->redirect(array('action' => 'cadastrarModalidade'));
        }

        $modalidadesArray = $this->Modalidade->generateTreeList();

        foreach ($modalidadesArray as $i => $modalidade) {
            $models = $this->Modalidade->getPath($i);
            $path = '';
            foreach ($models as $model) {
                $tipo = "";
                if($model['Modalidade']['tipo'] == 'I') {
                    $tipo = ' (Individual)';
                } else if($model['Modalidade']['tipo'] == 'D') {
                    $tipo = ' (Dupla)';
                } else if($model['Modalidade']['tipo'] == 'C') {
                    $tipo = ' (Coletivo)';
                }

                $sexo = "";
                if($model['Modalidade']['sexo'] == 'F') {
                    $sexo = ' (Feminino)';
                } else if($model['Modalidade']['sexo'] == 'M') {
                    $sexo = ' (Masculino)';
                }

                $path .= $model['Modalidade']['name'] . $tipo . $sexo . ' > ';
            }
            $path = substr($path, 0, strlen($path) - 3);
            $modalidadesArray[$i] = $path;
        }
        $this->request->data['Modalidade']['parent_id'] = $modalidadesArray;
    }

}
?>