<?php
class Delegacao extends AppModel {
    public $actsAs = array('Containable');
    
    public $name = 'Delegacao';
    public $useTable = 'delegacoes';
    public $belongsTo = array(
    					"Campus" => array(
    						"className"  => 'Campus',
    						"foreignKey" => 'campu_id'
    					),	
    					"Servidor" => array(
    						"className"  => 'Servidor',
    						"foreignKey" => 'servidor_id'
    					),
    				);
    public $hasMany = array(
        'ModalidadesAtleta' => array(
            'className' => 'ModalidadesAtleta',
            'foreignKey' => 'delegacao_id'
        )
    );
}
?>