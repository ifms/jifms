<?php
class Campus extends AppModel {

    public $name = 'Campus';
    public $useTable = 'campus';

    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('isUnique'),
                'message' => 'Nome deve ser unico'
                )
            ),
        );


}
?>