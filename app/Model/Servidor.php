<?php
class Servidor extends AppModel {

    public $useTable = 'servidores';
    public $belongsTo = array(
    					"Campus" => array(
    						"className"  => 'Campus',
    						"foreignKey" => 'campu_id'
    					),
    					"User" => array(
    						"className"  => 'User',
    						"foreignKey" => 'user_id'	
    					));	
}
?>