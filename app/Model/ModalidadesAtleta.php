<?php
class ModalidadesAtleta extends AppModel {

    public $name = 'ModalidadesAtleta';
    public $useTable = 'modalidades_atletas';
    public $belongsTo = array(
    					"Atleta" => array(
    						"className"  => 'Atleta',
    						"foreignKey" => 'atleta_id'
    					),	
    					"Modalidade" => array(
    						"className"  => 'Modalidade',
    						"foreignKey" => 'modalidade_id'
    					),
    				);

}
?>