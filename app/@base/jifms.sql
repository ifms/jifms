/*  BANCO  */

CREATE DATABASE IF NOT EXISTS `jifms` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jifms`;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `descricao` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT '',
  `foreign_key` int(10) unsigned DEFAULT NULL,
  `alias` varchar(255) DEFAULT '',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) unsigned NOT NULL,
  `aco_id` int(10) unsigned NOT NULL,
  `_create` char(2) NOT NULL DEFAULT '0',
  `_read` char(2) NOT NULL DEFAULT '0',
  `_update` char(2) NOT NULL DEFAULT '0',
  `_delete` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fullname` VARCHAR(150) NOT NULL,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `active` INT(1) NULL DEFAULT '1',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS roles(
  id  INT NOT NULL AUTO_INCREMENT,
  papel VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
);
/* INSERT roles */
INSERT INTO roles (papel) VALUE ('Administrador');
INSERT INTO roles (papel) VALUE ('Gestor Local');
INSERT INTO roles (papel) VALUE ('Professor');
INSERT INTO roles (papel) VALUE ('Arbitragem');
INSERT INTO roles (papel) VALUE ('Suporte');

CREATE TABLE IF NOT EXISTS users_roles(
  id INT NOT NULL AUTO_INCREMENT,
  role_id INT NOT NULL,
  user_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(role_id) REFERENCES roles(id),
  FOREIGN KEY(user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS `campus` (
  `id`      INT NOT NULL AUTO_INCREMENT,
  `nome`    VARCHAR(512) NOT NULL,
  `cidade`  VARCHAR(1024) NULL,
  `estado`  VARCHAR(120) NULL,
  PRIMARY KEY (`id`),
  UNIQUE (`nome`)
);

CREATE TABLE IF NOT EXISTS `servidores` (
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id` INT NULL REFERENCES user(`id`),
  `campu_id` INT NULL REFERENCES campus(`id`),
  `nome` VARCHAR(512) NOT NULL,
  `jif` INT(1) NOT NULL DEFAULT 1,
  `funcao` VARCHAR(512) NOT NULL,
  `sexo` CHAR(1) NOT NULL,
  `nascimento` DATE NULL,
  `telefone` VARCHAR(16) NULL,
  `siape` INT NULL,
  `rg` VARCHAR(64) NULL,
  `rg_expedidor` VARCHAR(8) NULL,
  `cpf` VARCHAR(64) NULL,
  `email` VARCHAR(512) NULL,
  `tipo_sanguineo` VARCHAR(64) NULL,
  `alergia` VARCHAR(512) NULL,
  `medicamentos` VARCHAR(512) NULL,
  `doenca_cronica` VARCHAR(512) NULL,
  `observacoes` TEXT NULL,
  `imagem` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`campu_id`) REFERENCES campus(`id`),
  FOREIGN KEY (`user_id`) REFERENCES users(`id`)
);
-- alter table servidores add column imagem varchar(255) null;

CREATE TABLE IF NOT EXISTS `servidores_modalidades` (
  `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `servidor_id` INT NOT NULL REFERENCES servidores(`id`),
  `modalidade_id` INT NOT NULL REFERENCES modalidades(`id`)
);

CREATE TABLE IF NOT EXISTS `delegacoes` (
  `id`        INT NOT NULL AUTO_INCREMENT,
  `campu_id`  INT NOT NULL,
  `nome`      VARCHAR(1024) NULL,
  `servidor_id`   INT NULL,
  `active`    INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`campu_id`) REFERENCES campus(`id`),
  FOREIGN KEY (`servidor_id`) REFERENCES servidores(`id`)
);
-- alter table delegacoes add column active int(1) not null default 1;

CREATE TABLE IF NOT EXISTS `modalidades` (
  `id`          INT NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(512) NOT NULL,
  `parent_id`   INT NULL,
  `tipo`        CHAR(1) NULL,
  `sexo` char(1) NULL,
  `lft` INTEGER(10) DEFAULT NULL,
  `rght` INTEGER(10) DEFAULT NULL,
  `active`    INT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
);
-- alter table modalidade add column active int(1) not null default 1;

INSERT INTO `modalidades` (`id`, `name`, `parent_id`, `tipo`, `sexo`, `lft`, `rght`) VALUES
(8, 'Atletismo', NULL, NULL, '', 1, 22),
(9, '100 Metros', 8, 'I', 'M', 2, 3),
(10, '200 Metros', 8, 'I', 'M', 4, 5),
(11, '400 Metros', 8, 'I', 'M', 6, 7),
(12, '800 Metros', 8, 'I', 'M', 8, 9),
(13, '1500 Metros', 8, 'I', 'M', 10, 11),
(14, 'Revezamento 4x100 Metros', 8, 'I', 'M', 12, 13),
(15, 'Revezamento Medley', 8, 'I', 'M', 14, 15),
(16, 'Salto em DistÃ¢ncia', 8, 'I', 'M', 16, 17),
(17, 'Salto em Altura', 8, 'I', 'M', 18, 19),
(18, 'Arremesso de Peso', 8, 'I', 'M', 20, 21),
(19, 'Basquete', 0, 'C', 'M', 23, 24),
(20, 'Futsal', 0, 'C', 'M', 25, 26),
(21, 'Futsal', 0, 'C', 'F', 27, 28),
(22, 'Volei de Areia', 0, 'I', 'F', 29, 30),
(23, 'Xadrez', 0, 'I', 'M', 31, 32),
(24, 'Xadrez', 0, 'I', 'F', 33, 34);

CREATE TABLE IF NOT EXISTS `atletas` (
  `id`    INT NOT NULL AUTO_INCREMENT,
  `campu_id` INT NOT NULL,
  `nome`  VARCHAR(512) NOT NULL,
  `sexo`  CHAR(1) NOT NULL,
  `data_nascimento` DATE NOT NULL,
  `telefone` VARCHAR(100) NULL,
  `jif` INT(1) NOT NULL DEFAULT 1,
  `banco` VARCHAR(10) NULL,
  `banco_op` VARCHAR(10) NULL,
  `banco_ag` VARCHAR(20) NULL,
  `banco_conta` VARCHAR(30) NULL,
  `tamanho_camisa` VARCHAR(5) NULL,
  `tamanho_short` VARCHAR(5) NULL,
  `rg` VARCHAR(30) NULL,
  `rg_exped` DATE NULL,
  `rg_orgexped` VARCHAR(10) NULL,
  `cpf` VARCHAR(30) NULL,
  `email` VARCHAR(100) NULL,
  `tipagem_sangue` VARCHAR(5) NULL,
  `altura` DECIMAL(9,2) NULL,
  `massa` DECIMAL(9,2) NULL,
  `reacao_alergica` TINYTEXT NULL,
  `medicamentos_uso` TINYTEXT NULL,
  `doenca_cronica` TINYTEXT NULL,
  `curso_ifms` VARCHAR(120) NULL,
  `semestre_ingresso` VARCHAR(50) NULL,
  `turno` VARCHAR(100) NULL,
  `matricula` VARCHAR(100) NULL,
  `endereco` TINYTEXT NULL,
  `cidade` VARCHAR(255) NULL,
  `responsavel` VARCHAR(255) NULL,
  `email_responsavel` VARCHAR(100) NULL,
  `telefone_responsavel` VARCHAR(100) NULL,
  `federado` INT(1) NOT NULL DEFAULT 0,
  `periodo_treinamento` VARCHAR(20) NULL,
  `obs` TEXT NULL,
  `active`    INT(1) NOT NULL DEFAULT 1,
  `imagem` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`campu_id`) REFERENCES campus (`id`)  
);
-- alter table atletas add column active int(1) not null default 1;
-- alter table atletas add column imagem varchar(255) null;

CREATE TABLE IF NOT EXISTS `modalidades_atletas` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `atleta_id` INT NOT NULL,
  `modalidade_id` INT NOT NULL,
  `delegacao_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`atleta_id`) REFERENCES atletas(`id`),
  FOREIGN KEY (`modalidade_id`) REFERENCES modalidades(`id`),
  FOREIGN KEY (`delegacao_id`) REFERENCES delegacoes(`id`),
  UNIQUE(`atleta_id`, `modalidade_id`, `delegacao_id`)
);

CREATE TABLE IF NOT EXISTS  `confrontos` (
 `id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `modalidade_id` INT( 11 ) NOT NULL ,
 `data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
 `delegacao_a` INT( 11 ) NOT NULL ,
 `delegacao_b` INT( 11 ) NOT NULL ,
 `local` VARCHAR( 512 ) NOT NULL ,
 `numero` INT( 11 ) NOT NULL
) ENGINE = INNODB DEFAULT CHARSET = latin1
